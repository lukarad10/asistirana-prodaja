(function () {

  function Calculator(initObj) {
    this.initObj = initObj;

    this.initComponents();
    this.setInitialValues(initObj);

    console.log(initObj);
  }

  Calculator.prototype.initComponents = function() {
    var instance = this;

    $('.addiko-input-number').bind('input', function(ev) {
      $(this).val(function(){
        var val = $(this).val() || "0";

        return parseInt(val.replace(/[^0-9]+/g, '')).toLocaleString('sr-RS');
      });
    });

    $(".js-amount.textual-input").blur(function(ev) {
      instance.changeEvent(ev);
    });

    $(".js-amount").on("input", function(ev) {
      instance.changeEvent(ev);
    });

    $(".js-deadline").on("input", function(ev) {
      instance.changeEvent(ev);
    });

    $('#calculator-form').submit(function() {
      $('#hidden-amount-fcl').val(parseFloat($('#calc-ex-fcl-amount').text().replace(".", "")));
      $('#hidden-months-fcl').val($('#calc-ex-fcl-deadline').text());

      var $hiddenAnnuityFCL = $('#calc-ex-fcl-payment').text();
      $hiddenAnnuityFCL = $hiddenAnnuityFCL.replace(".", "");
      $hiddenAnnuityFCL = parseFloat($hiddenAnnuityFCL.replace(",", "."));
      $('#hidden-annuity-fcl').val($hiddenAnnuityFCL);

      // $('#hidden-interaction-fcl').val($('#user-interaction-fcl').attr('data-interaction'));

      $('#hidden-amount-2u1').val(parseFloat($('#calc-ex-2u1-amount').text().replace(".", "")));
      $('#hidden-months-2u1').val($('#calc-ex-2u1-deadline').text());

      var $hiddenAnnuity2u1 = $('#calc-ex-2u1-payment').text();
      $hiddenAnnuity2u1 = $hiddenAnnuity2u1.replace(".", "");
      $hiddenAnnuity2u1 = parseFloat($hiddenAnnuity2u1.replace(",", "."));
      $('#hidden-annuity-2u1').val($hiddenAnnuity2u1);

      // $('#hidden-interaction-2u1').val($('#user-interaction-2u1').attr('data-interaction'));

      if ($('#nav-fcl').hasClass('active')) {
        $('#hidden-active-calculator').val('fcl');
      } else if ($('#nav-2u1').hasClass('active')) {
        $('#hidden-active-calculator').val('2u1')
      }

      $(this).submit();
    });
  };

  Calculator.prototype.changeEvent = function(ev) {
    // debugger;
    var credit_type = $(ev.currentTarget).attr("data-type");
    var loan_data = this.initObj.loans[credit_type];

    var deadline = $("input.js-deadline[data-type=" + credit_type + "]").val();

    if($(ev.currentTarget).hasClass("textual-input") ) {
      var amount = $(ev.currentTarget).val();
      amount = amount.replace(/\./g, "");

      if(ev.type === "blur") {
        amount = this.correctValues(loan_data, credit_type, amount).toString();
      }
    } else {
      amount =  $("input.js-amount.range-input[data-type=" + credit_type + "]").val();
    }

    amount = amount.replace(/\./g, "");

    this.correctBounds(loan_data, credit_type, amount, deadline);

    var interest = this.calculateInterest(loan_data, amount, deadline);
    var result = this.calculateEverything(interest, amount, deadline, loan_data.initial_fee, loan_data.menica);

    console.log(result);

    this.setValues(credit_type, result);
  };

  Calculator.prototype.setInitialValues = function(init_obj) {
    var init_values = init_obj.initial_values;

    for(var index in init_values) {
      var obj = init_values[index];
      var months_bound = init_obj.loans[index].months_bounds;

      console.log(months_bound);

      $(".js-amount[data-type=" + index + "]").attr("step", obj.amount_step).val(obj.amount).trigger("input");
      $(".js-deadline[data-type=" + index + "]").attr("step", obj.deadline_step).val(obj.amount).trigger("input");

      $(".js-amount[data-type=" + index + "]").attr("min", months_bound.min);
      $(".js-amount[data-type=" + index + "]").attr("max", months_bound.max);

      $(".js-amount[data-type=" + index + "]").val(obj.amount).trigger("input");
      $(".js-deadline[data-type=" + index + "]").val(obj.deadline).trigger("input");


    }
  };

  Calculator.prototype.correctValues = function(loan_data, credit_type, amount) {
    var min = 1000000000;
    var max = 0;
    for(var i=0; i<loan_data.months_bounds.length; i++) {
      if(loan_data.months_bounds[i].min_amount < min) min = loan_data.months_bounds[i].min_amount;
      if(loan_data.months_bounds[i].max_amount > max) max = loan_data.months_bounds[i].max_amount;
    }

    if(amount < min) {
      amount = min;
    }

    if(amount > max) {
      amount = max;
    }
    return amount;
  };

  Calculator.prototype.getBounds = function(loan_data, credit_type, amount) {
    if(loan_data.hasOwnProperty("months_bounds")) {
      for(var i=0; i<loan_data.months_bounds.length; i++) {
        if(amount >= loan_data.months_bounds[i].min_amount && amount <= loan_data.months_bounds[i].max_amount) {
          return {
            min: loan_data.months_bounds[i].min,
            max: loan_data.months_bounds[i].max
          };
        }
      }
    }

    return null;
  };

  Calculator.prototype.correctBounds = function(loan_data, credit_type, amount) {
    var bounds = this.getBounds(loan_data, credit_type, amount);

    if(bounds) {
      $(".js-deadline[data-type=" + credit_type + "]").attr("min", bounds.min).attr("max", bounds.max);
    }
  };

  Calculator.prototype.setValues = function(type, result) {
    $(".js-amount[data-type = " + type + "]").val(result.amount);
    $(".js-amount.textual-input[data-type = " + type + "]").val(this.eRs(result.amount));
    $(".js-amount.js-div[data-type = " + type + "]").text(this.eRs(result.amount));

    $(".js-deadline[data-type = " + type + "]").val(result.deadline);
    $(".js-deadline.js-div[data-type =" + type + "]").html(result.deadline);

    $(".js-annuity[data-type = " + type + "]").html(this.eRs(result.annuity));

    $(".js-interest[data-type = " + type + "]").html(this.eRs(result.interest));

    $(".js-fee[data-type = " + type + "]").html(this.eRs(result.fee));
    $(".js-fee-percent[data-type = " + type + "]").html(result.fee_percent);
    $(".js-total[data-type = " + type + "]").html(this.tableRs(parseFloat(result.total_loan) + parseFloat(result.fee) + parseFloat(result.menica)));

    console.log(result.total_loan);
    console.log(result.fee);
  };

  Calculator.prototype.calculateInterest = function(loan, amount, deadline) {
    for(var i=0; i<loan.interests.length; i++) {
      if(amount >= loan.interests[i].min_amount && amount <= loan.interests[i].max_amount) {
        if(loan.interests[i].hasOwnProperty("interest")) {
          return {
            interest: loan.interests[i].interest,
            obj: loan.interests[i]
          };
        } else if(loan.interests[i].hasOwnProperty("interests")) {
          for(var j= 0; j < loan.interests[i].interests.length; j++) {
            if(deadline >= loan.interests[i].interests[j].min_deadline && deadline <= loan.interests[i].interests[j].max_deadline) {
              return {
                interest: loan.interests[i].interests[j].interest,
                obj: loan.interests[i].interests[j]
              };
            }
          }
        }
      }
    }

    return null;
  };

  Calculator.prototype.calculateEverything = function(interest, amount, deadline, initial_fee, menica) {

    if(interest.obj.hasOwnProperty("fee")) {
      var fee = interest.obj.fee;
    } else {
      fee = initial_fee;
    }

    var instance = this;

    return {
      menica: menica,
      amount: amount,
      deadline: deadline,
      interest: interest.interest,
      fee: instance.fee(amount, fee),
      fee_percent: fee,
      annuity: instance.payment(interest.interest, deadline, amount),
      total_loan: instance.total(interest.interest, deadline, amount),
      total_to_pay: instance.total(interest.interest, deadline, amount)
    }
  };

  Calculator.prototype.fee = function(amount, fee) {
    return amount * fee / 100;
  };

  // localize number
  Calculator.prototype.eRs = function(x){
    if(isNaN(parseFloat(x))) {
      return "";
    } else {
      return parseFloat(x).toLocaleString('sr-RS');
    }
  };

  Calculator.prototype.tableRs = function(x){
    if(isNaN(parseFloat(x))) {
      return "";
    } else {
      return parseFloat(x).toLocaleString('sr-RS');
    }
  };

  /**
   * Function that returns monthly payment
   *
   * @param interest
   * @param deadline
   * @param amount
   * @returns {string}
   */
  Calculator.prototype.payment = function(interest, deadline, amount) {
    interest /= 100;
    interest /= 12;

    var ret = ((amount * Math.pow((1 + interest), deadline) * interest) / ((Math.pow((1 + interest), deadline)) - 1));
    console.log(ret);

    if(isNaN(ret)) {
      ret = "";
    } else {
      ret = ret.toFixed(2)
    }

    console.log(ret);

    return ret;
  };


  /**
   * return bank's initial fee
   *
   * @param amount
   * @returns {string} initial fee that is payed once
   */
  // Calculator.prototype.fee = function(amount, fee) {
  //     return this.eRs((amount * fee).toFixed(2));
  // };

  /**
   * returns total to pay
   *
   * @param interest
   * @param deadline
   * @param amount
   * @returns {string}
   */
  Calculator.prototype.total = function(interest, deadline, amount) {
    interest /= 100;
    interest /= 12;

    return (deadline * (amount * Math.pow((1 + interest), deadline) * interest) / ((Math.pow((1 + interest), deadline)) - 1)).toFixed(2);
  };





  window.Calculator = Calculator;

})();