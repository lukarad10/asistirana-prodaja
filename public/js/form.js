$(document).ready(function() {
  var submit_ready = false;

  // grecaptcha.execute(ADDIKO_RECAPTCHA_SITE_KEY, {
  //     action: 'welcome',
  //     'badge': 'inline',
  // }).then(function(token) {
  //     // console.log(token);
  //
  //     $("#recaptcha").val(token);
  //     // $(form).submit();
  // });

  $("#validation-form").submit(function(event) {
    var form = this;

    if(!submit_ready) {
      event.preventDefault();

      grecaptcha.execute(ADDIKO_RECAPTCHA_SITE_KEY, {
        action: 'welcome'
      }).then(function(token) {
        console.log(token);

        $("#recaptcha").val(token);
        $(form).submit();
      });

      submit_ready = true;
    }
  });

  var $kreditni_biro_checkbox = $("#kreditni-biro-consent");

  $("#kreditni-biro-consent-label").click(function(ev) {
    ev.stopPropagation();
    ev.preventDefault();
    $('#kreditni-biro-consent-modal').modal({});
  });

  $("#btn-kreditni-biro-consent-yes").click(function() {
    $kreditni_biro_checkbox.prop("checked", true);

    var $kreditni_biro_consent_error = $(".kreditni-biro-consent-error");

    if($kreditni_biro_consent_error.length) {
      $kreditni_biro_consent_error.remove();
    }
  });

  $("#btn-kreditni-biro-consent-no").click(function() {
    $kreditni_biro_checkbox.prop("checked", false);

    var $kreditni_biro_consent_error = $(".kreditni-biro-consent-error");
    if(!$kreditni_biro_consent_error.length) {
      $("#kreditni-biro-consent-label").after('<div class="invalid-feedback addiko-checkbox-radio-btn-error kreditni-biro-consent-error">Ovo polje je obavezno.</div>');
    }
  });



  var $general_consent_checkbox = $("#general-consent");

  $("#general-consent-label").click(function(ev) {
    ev.stopPropagation();
    ev.preventDefault();
    $('#general-consent-modal').modal({});
  });

  $("#btn-general-consent-yes").click(function() {
    $general_consent_checkbox.prop("checked", true);

    // var $general_consent_error = $(".general-consent-error");
    //
    // if($general_consent_error.length) {
    //   $general_consent_error.remove();
    // }
  });

  $("#btn-general-consent-no").click(function() {
    $general_consent_checkbox.prop("checked", false);

    // var $general_consent_error = $(".general-consent-error");
    //
    // if(!$general_consent_error.length) {
    //   $("#general-consent-label").after('<div class="invalid-feedback addiko-checkbox-radio-btn-error general-consent-error">Ovo polje je obavezno.</div>');
    // }
  });

  $('.expositure').select2({
    minimumResultsForSearch: Infinity
  });

});