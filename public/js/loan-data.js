var loan_data = {
  initial_values: {
    fcl: {
      amount: 2000,
      deadline: 36,
      amount_step: 500,
      deadline_step: 6
    },
    "2u1": {
      amount: 18000,
      deadline: 72,
      amount_step: 500,
      deadline_step: 6
    }
  },

  loans: {
    fcl: {
      initial_fee: 0,
      menica: 2,

      months_bounds: [
        {
          min: 12,
          max: 60,
          min_amount: 1000,
          max_amount: 3000
        },
        {
          min: 12,
          max: 72,
          min_amount: 3001,
          max_amount: 7000
        }
      ],
      interests: [
        {
          min_amount: 1000,
          max_amount: 3000,
          interest: 14.99,
          fee: 5,
        },
        {
          min_amount: 3001,
          max_amount: 7000,
          interest: 11.49,
          fee: 2,
        }
      ]
    },
    "2u1": {
      initial_fee: 1.8,
      menica: 2,

      months_bounds: [
        {
          min_amount: 1000,
          max_amount: 30000,

          min: 12,
          max: 72
        }
      ],
      interests: [
        {
          min_amount: 1000,
          max_amount: 5000,
          interest: 8.99,
          fee: 1.25
        },
        {
          min_amount: 5001,
          max_amount: 17000,
          interest: 7.99,
          fee: 1.25
        },
        {
          min_amount: 17001,
          max_amount: 20000,
          interest: 7.59,
          fee: 1.25
        },
        {
          min_amount: 20001,
          max_amount: 30000,
          interest: 6.99,
          fee: 1.25
        }
      ]
    }
  }
};