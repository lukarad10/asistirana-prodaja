<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property database_model $database_model
 */
class Registration extends CIR_MainController {
//    protected $current_position = ADDIKO_POSITION_WELCOME;
    private $recaptcha_result;

	public function index()
	{
	    $offices = $this->database_model->get_offices();

        if($this->input->post()) {

            $this->form_validation->set_rules($this->get_form_validation_object());

            if ($this->form_validation->run() == FALSE) {

                $this->twig->display("form", [
                    "body_class" => "form",
                    "offices"    => $offices
                ]);
            } else {
                $recaptcha_result = $this->recaptcha();

                if(!$recaptcha_result) {
                    $this->twig->display("form", [
                        "body_class"        => "form",
                        "recaptcha_error"   => 1,
                        "offices"           => $offices
                    ]);

                    return;
                }

                $temp_uuid = $_SESSION[ADDIKO_TEMP_USER_UUID];

                //TODO ubaciti proveru da li lead vec postoji u tabeli i  da li ima pravo da unese jos jednom sebe u bazu

                $kreditni_biro_consent = $this->input->post("kreditni-biro-consent", true);
                if(!$kreditni_biro_consent) $kreditni_biro_consent = 0;

                $general_consent = $this->input->post("general-consent", true);
                if(!$general_consent) $general_consent = 0;

                $this->database_model->insert_temp_lead_and_send_otp(
                    $this->input->post("firstname", true),
                    $this->input->post("lastname", true),
                    $this->input->post("jmbg", true),
                    $this->input->post("email", true),
                    format_mobile_number($this->input->post("mobile", true)),
                    $this->input->post("expositure", true),
                    $kreditni_biro_consent,
                    $general_consent,
                    $temp_uuid,
                    $this->recaptcha_result
                );

                $_SESSION["ADDIKO_NEXT_POSITION"] = ADDIKO_POSITION_VALIDATION;

                redirect(base_url(ADDIKO_ROUTE_VALIDATION));
            }
        } else {
//            unset($_SESSION[ADDIKO_TEMP_USER_UUID]);
//            unset($_SESSION[ADDIKO_USER_UUID]);
//            unset($_SESSION["ADDIKO_POSITION"]);
//            unset($_SESSION["ADDIKO_NEXT_POSITION"]);

            $this->twig->display("form", [
                "offices"    => $offices
            ]);
        }
	}

	private function get_form_validation_object() {
        return [
            [
                'field'  => 'firstname',
                'label'  => 'Ime',
                'rules'  => [
                    'trim',
                    'required',
                    'regex_match[/^[A-ZŠĐČĆŽ][a-zšđčćž A-ZŠĐČĆŽ]{2,}$/]',
                    'max_length[50]'
                ],
                'errors' => [
                    'required'    => 'Ovo polje je obavezno.',
                    'regex_match' => '%s mora imati početno veliko  slovo i ne sme sadržati brojeve.',
                    'max_length' => 'Najveći broj dozvoljenih znakova je 50.'
                ]
            ],
            [
                'field'  => 'lastname',
                'label'  => 'Priimek',
                'rules'  => [
                    'trim',
                    'required',
                    'regex_match[/^[A-ZŠĐČĆŽ][a-zšđčćž A-ZŠĐČĆŽ]{2,}$/]',
                    'max_length[50]'
                ],
                'errors' => [
                    'required'    => 'Ovo polje je obavezno.',
                    'regex_match' => '%s mora imati početno veliko slovo i ne sme sadržati brojeve.',
                    'max_length' => 'Najveći broj dozvoljenih znakova je 50.'
                ]
            ],
            [
                'field'  => 'jmbg',
                'label'  => 'JMBG',
                'rules'  => [
                    'trim',
                    'required',
                    // 'regex_match[/^[A-ZŠĐČĆŽ][a-zšđčćž A-ZŠĐČĆŽ]{2,}$/]',
                    'max_length[13]',
                    'min_length[13]'
                ],
                'errors' => [
                    'required'    => 'Ovo polje je obavezno.',
                    // 'regex_match' => '%s mora imeti veliko začetno črko in ne sme vsebovati številk.',
                    'min_length' => '%s ne sme sadržati manje od 13 cifara.',
                    'max_length' => '%s ne sme sadržati više od 13 cifara'
                ]
            ],
            [
                'field'  => 'email',
                'label'  => 'E-mail',
                'rules'  => [
                    'trim',
                    'required',
                    'valid_email',
                    'max_length[90]'
                ],
                'errors' => [
                    'required'    => 'Ovo polje je obavezno.',
                    'valid_email' => '%s nije validan.',
                    'max_length' => 'Najveći broj dozvoljenih znakova je 50.'
                ]
            ],
            [
                'field'  => 'mobile',
                'label'  => 'Telefon',
                'rules'  => [
                    'required',
                    'regex_match[/^(\+3826|003826|06)[0-9]\d{6,7}$/]',
                    'max_length[15]'
                ],
                'errors' => [
                    'required'    => 'Ovo polje je obavezno.',
                    'regex_match' => 'Telefon nije validan.',
                    'max_length' => 'Najveći broj dozvoljenih znakova je 20.'
                ],
            ],
            // '/^(\+387|00387|0)6[0-9]\d{6,7}$/'

            [
                'field'  => 'kreditni-biro-consent',
                'label'  => 'Lični podaci',
                'rules'  => [
                    'required',
                    'regex_match[/^1/]'
                ],
                'errors' => [
                    'required'    => 'Ovo polje je obavezno.',
                    'regex_match'    => 'Ovo polje je obavezno.'
                ]
            ],
            [
                'field'  => 'general-consent',
                'label'  => 'Uslovi',
                'rules'  => [
                    'regex_match[/^1/]'
                ],
                'errors' => [
                    'regex_match'    => 'Ovo polje je obavezno.'
                ]
            ]
        ];
    }

    private function recaptcha() {
        // reCAPTCHA
        $post_data = http_build_query(
            [
                'secret' => ADDIKO_RECAPTCHA_SECRET_KEY,
                'response' => (isset($_POST['recaptcha'])) ? $_POST['recaptcha'] : '',
                'remoteip' => $_SERVER['REMOTE_ADDR']
            ]
        );
        $opts = ['http' =>
            [
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $post_data
            ]
        ];

        $context  = stream_context_create($opts);

        $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
        $result = json_decode($response, true); // brisanje komentara i ovog ispod za aktiviranje reCAPTCHe

        $this->recaptcha_result = $result;

        if($result["success"] == true && $result["action"] == "welcome") {
              var_dump(ADDIKO_CAPTCHA_CONFIDENCE);
            return $result["score"] >= ADDIKO_CAPTCHA_CONFIDENCE;
        } else {
            return false;
        }
    }
} // end class


