<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property database_model $database_model
 */
class Calculator extends CIR_MainController
{
      protected $current_position = ADDIKO_POSITION_WELCOME;

      public function index()
      {
            if($this->input->post()) {
//                  $this->form_validation->set_rules($this->get_form_validation_object());

//                  if ($this->form_validation->run() == FALSE) {
//                        $this->twig->display("calculator");
//                  } else {
                        // ubaci podatke u bazu
                        $temp_uuid = uniqid("", true);
                        $_SESSION[ADDIKO_TEMP_USER_UUID] = $temp_uuid;

                        $this->database_model->insert_row("calculator", ["uuid", "amount_fcl", "months_fcl", "annuity_fcl",
                            "amount_two_in_one", "months_two_in_one", "annuity_two_in_one", "active_loan"], [
                            $temp_uuid,
                            $this->input->post("hidden-amount-fcl", true),
                            $this->input->post("hidden-months-fcl", true),
                            $this->input->post("hidden-annuity-fcl", true),
                            $this->input->post("hidden-amount-2u1", true),
                            $this->input->post("hidden-months-2u1", true),
                            $this->input->post("hidden-annuity-2u1", true),
                            $this->input->post("hidden-active-calculator", true)
                        ]);

                        redirect(base_url(ADDIKO_ROUTE_REGISTRATION));
//                  }
            } else {
                  $this->twig->display("calculator");
            }

      }

      //napraviti regex za iznos kredita i period otplate
      // nakon toga otkomentarisati onu validaciju iznad
//      private function get_form_validation_object() {
//            return [
//                [
//                    'field'  => 'hidden-amount',
//                    'rules'  => [
//                        'trim',
//                        'required',
//                        'regex_match[/^[A-ZŠĐČĆŽ][a-zšđčćž A-ZŠĐČĆŽ]{2,}$/]',
//                    ],
//                    'errors' => [
//                        'required'    => 'To polje je obvezno.',
//                        'regex_match' => '%s mora imeti veliko začetno črko in ne sme vsebovati številk.',
//                        'max_length' => 'Največje število znakov je 50'
//                    ]
//                ],
//                [
//                    'field'  => 'hidden-months',
//                    'rules'  => [
//                        'trim',
//                        'required',
//                        'regex_match[/^[A-ZŠĐČĆŽ][a-zšđčćž A-ZŠĐČĆŽ]{2,}$/]',
//                    ],
//                    'errors' => [
//                        'required'    => 'To polje je obvezno.',
//                        'regex_match' => '%s mora imeti veliko začetno črko in ne sme vsebovati številk.',
//                        'max_length' => 'Največje število znakov je 50'
//                    ]
//                ],
//                [
//                    'field'  => 'hidden-annuity',
//                    'rules'  => [
//                        'trim',
//                        'required',
//                        'regex_match[/^[A-ZŠĐČĆŽ][a-zšđčćž A-ZŠĐČĆŽ]{2,}$/]',
//                    ],
//                    'errors' => [
//                        'required'    => 'To polje je obvezno.',
//                        'regex_match' => '%s mora imeti veliko začetno črko in ne sme vsebovati številk.',
//                        'max_length' => 'Največje število znakov je 50'
//                    ]
//                ],
//                [
//                    'field'  => 'user-interaction',
//                    'rules'  => [
//                        'trim',
//                        'required',
//                        'regex_match[/^[A-ZŠĐČĆŽ][a-zšđčćž A-ZŠĐČĆŽ]{2,}$/]',
//                    ],
//                    'errors' => [
//                        'required'    => 'To polje je obvezno.',
//                        'regex_match' => '%s mora imeti veliko začetno črko in ne sme vsebovati številk.',
//                        'max_length' => 'Največje število znakov je 50'
//                    ]
//                ]
//            ];
//      }
}