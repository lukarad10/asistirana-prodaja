<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property database_model $database_model
 */
class Validation extends CIR_MainController{
    protected $current_position = ADDIKO_POSITION_VALIDATION;

    public function index()
    {
        if(!isset($_SESSION[ADDIKO_TEMP_USER_UUID])) redirect(base_url(ADDIKO_ROUTE_SESSION_EXPIRED));

        if ($this->input->post()) {
            $this->form_validation->set_rules($this->get_form_validation_object());

            if ($this->form_validation->run() == FALSE) {

                $this->twig->display("validation", [
                    "body_class" => "validation"
                ]);
            } else {
                // TODO  umesto update temp lead treba da procitas temp lead i da ga sa prepises u leads tabelu
                // postoji funcija get_temp_lead_by_uuid koja vraca temp lead
                  // get the lead from verify table
                  $temp_lead = $this->database_model->get_temp_lead_by_uuid($_SESSION[ADDIKO_TEMP_USER_UUID]);
                  $calc = $this->database_model->get_calc_by_uuid($_SESSION[ADDIKO_TEMP_USER_UUID]);

                  if(!$temp_lead || !$calc) {
                        // for some reason lead anddoesn't exists
                        show_404();
                  } else {
                        $new_uuid = $this->database_model->insert_lead($temp_lead, $calc);
                        $this->database_model->insert_lead_to_mautic($temp_lead);

                        $this->send_mail($temp_lead, $calc);

                        redirect(base_url(ADDIKO_ROUTE_INFO));
                  }

//                  $delete_temp_data = $this->database_model->delete_temp_data($new_uuid);

            }
        } else {
            $this->twig->display("validation");
        }
    }

    private function send_mail($temp_lead, $calc) {
        $template = file_get_contents("application/views/email_template.html");

        if($calc["active_loan"] == "fcl") {
            $message = str_replace("%%loan%%", "Super keš kredit", $template);
            $message = str_replace("%%amount%%", $calc["amount_fcl"], $message);
            $message = str_replace("%%period%%", $calc["months_fcl"], $message);
            $message = str_replace("%%anuity%%", $calc["annuity_fcl"], $message);
        } else {
            $message = str_replace("%%loan%%", "2u1 keš kredit", $template);
            $message = str_replace("%%amount%%", $calc["amount_two_in_one"], $message);
            $message = str_replace("%%period%%", $calc["months_two_in_one"], $message);
            $message = str_replace("%%anuity%%", $calc["annuity_two_in_one"], $message);
        }

        $message =  str_replace("%%url%%", base_url("email-validacija/") . $temp_lead["uuid"], $message);

        send_html_mail(
            "Asistirana prodaja",
            "Asistirana prodaja <asistirana.prodaja.me@addiko.com>",
            "Zahtjev za kredit",
            $message,
            $temp_lead["email"]
        );
    }

    private function get_form_validation_object()
    {
        return [
            [
                'field' => 'otp',
                'label' => 'Kod',

                'rules' => [
                    'required',
                    'min_length[6]',
                    'max_length[6]',
                    ['code_valid', function($string) {
                        if($this->database_model->check_if_sms_code_valid($string)) {
//                            echo "valid code";
                            return true;
                        } else {
                            return false;
                        }
                    }],
                    ['code_expired', function($string) {
                        if($this->database_model->check_if_sms_code_expired($string)) {
//                            echo "expired code";
                            return true;
                        } else {
                            return false;
                        }
                    }],
                ],

                'errors' => [
                    'required' => 'Ovo polje je obavezno.',
                    'min_length' => '%s mora sadržati 6 brojeva.',
                    'max_length' => '%s mora sadržati 6 brojeva.',
                    'code_valid' => '{field} je istekla i nije validna.',
                    'code_expired' => '{field} je istekla.'
                ]
            ]
        ];
    }

    public function xcode_valid($string) {
        echo "ASDASDASDASDASDD";

        if($this->database_model->check_if_sms_code_valid($string)) {
            $this->form_validation->set_message('code_expire', '{field} je istekla.');
            return false;
        } else {
            return true;
        }
    }

    /**
     * Code look up
     *
     * Chek code expiration.
     *
     * @version 1.0.0
     * @since 1.0.0
     * @param string $string Code to look up
     * @return boolean
     */
    public function code_expire($string)
    {
        if($this->database_model->check_if_sms_code_expired($string)) {
            $this->form_validation->set_message('code_expire', '{field} je istekla.');
            return false;
        } else {
            return true;
        }
    } // end function
} // end class
