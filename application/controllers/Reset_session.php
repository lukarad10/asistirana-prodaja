<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset_session extends CIR_Controller {

	public function index()
	{
        unset($_SESSION[ADDIKO_TEMP_USER_UUID]);
        unset($_SESSION[ADDIKO_USER_UUID]);
        unset($_SESSION["ADDIKO_POSITION"]);
        unset($_SESSION["ADDIKO_NEXT_POSITION"]);
	}
}