<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property database_model $database_model
 */
class Email_verification extends CIR_MainController
{
    protected $current_position = ADDIKO_POSITION_INFO;

    public function index($uuid)
    {
        $user = $this->database_model->get_lead_by_uuid($uuid);
        $office = $this->database_model->get_branch_office($user["expositure"]);

        if($user) {
            if($user["email_verified"]) {
                $this->twig->display('already-verified');
            } else {
                // proveravamo da li je istekao period od 48h za validaciju email adrese
                $now = new DateTime();
                $expireDate = new DateTime(strtotime($user["created_at"]));
                $expireDate->add(new DateInterval("PT48H"));

                if($now > $expireDate) {
                    $this->twig->display("link-expired");
                } else {
                    $this->database_model->db->query("UPDATE leads SET email_verified=1 WHERE $uuid=?", [$uuid]);
                    $this->send_mail_to_office($user, $office);
                    $this->twig->display("thank-you");
                }
            }
        } else {
            $this->twig->display('email-doesnt-exist');
        }
    }

    private function send_mail_to_office($user, $office) {
        $template = file_get_contents("application/views/branch_email_template.html");

        if($user["loan_type"] == "fcl") {
            $message = str_replace("%%loan%%", "Super keš kredit", $template);
        } else {
            $message = str_replace("%%loan%%", "2u1 keš kredit", $template);
        }

        $message = str_replace("%%firstname%%", $user["firstname"], $message);
        $message = str_replace("%%lastname%%", $user["lastname"], $message);
        $message = str_replace("%%email%%", $user["email"], $message);
        $message = str_replace("%%mobile%%", $user["mobile"], $message);
        $message = str_replace("%%jmbg%%", $user["jmbg"], $message);

        $message = str_replace("%%amount%%", $user["amount"], $message);
        $message = str_replace("%%period%%", $user["months"], $message);
        $message = str_replace("%%annuity%%", $user["annuity"], $message);

        if(ADDIKO_TEST) {
            $to = $user["email"];
        } else {
            $to = $office["email"];
        }

        send_html_mail(
            "Asistirana prodaja",
            "Asistirana prodaja <asistirana.prodaja.me@addiko.com>",
            "Zahtjev za kredit",
            $message,
            $to
        );
    }
}