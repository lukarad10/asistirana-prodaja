<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property database_model $database_model
 */
class Info extends CIR_MainController
{
    protected $current_position = ADDIKO_POSITION_INFO;

    public function index()
    {
        $this->twig->display("info");
    }
}