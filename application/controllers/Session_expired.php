<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Session_expired extends CIR_Controller {

	public function index()
	{
        unset($_SESSION[ADDIKO_TEMP_USER_UUID]);
        unset($_SESSION[ADDIKO_USER_UUID]);

        $this->twig->display("session-expired", [
            "body_class" => "session-expired"
        ]);
	}
}