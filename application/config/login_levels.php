<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config["login_levels"] = [
    ['diff' => 0, 'reset_in' => 0],
    ['diff' => 0, 'reset_in' => 0],
    ['diff' => 0, 'reset_in' => 0],
    ['diff' => 3, 'reset_in' => 6],
    ['diff' => 5, 'reset_in' => 10],
    ['diff' => 10, 'reset_in' => 20],
    ['diff' => 30, 'reset_in' => 60],
    ['diff' => 60, 'reset_in' => 120],
    ['diff' => 180, 'reset_in' => 360]
];
