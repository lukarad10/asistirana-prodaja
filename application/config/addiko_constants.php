<?php
/**
 * Created by PhpStorm.
 * User: Goran
 * Date: 9/10/2019
 * Time: 11:43 AM
 */

/*
|--------------------------------------------------------------------------
| Application status
|--------------------------------------------------------------------------
|
| Used to control which environment will be used.
| Defines 3 possible choices:
|   -  ADDIKO_APP_STATUS_LOCAL for local environment
|   -  ADDIKO_APP_STATUS_TEST for test environment
|   -  ADDIKO_APP_STATUS_PRODUCTION for production environment
|
| Setting ADDIKO_APPLICATION_STATUS to one of these 3 constants  il line 29
| will set everything for one of these environments
|
*/

define("ADDIKO_APP_STATUS_LOCAL", "local");
define("ADDIKO_APP_STATUS_TEST", "test");
define("ADDIKO_APP_STATUS_PRODUCTION", "production");

define("ADDIKO_APPLICATION_STATUS", ADDIKO_APP_STATUS_LOCAL);


/*
|--------------------------------------------------------------------------
| Positions in application
|--------------------------------------------------------------------------
|
| Used to control back button and trying to get to some step before
| completing previous ones
|
*/


define("ADDIKO_POSITION_NOT_SET", -1);
define("ADDIKO_POSITION_WELCOME", 0);
define("ADDIKO_POSITION_VALIDATION", 1);
define("ADDIKO_POSITION_SEGMENTATION", 2);
define("ADDIKO_POSITION_INFO", 3);
define("ADDIKO_POSITION_REGARDS", 4);

define("ADDIKO_SESSION_REDIRECTIONS", ["welcome", "validation", "segmentation", "info", "regards"]);


/*
|--------------------------------------------------------------------------
| Recaptcha v3 keys
|--------------------------------------------------------------------------
|
| Different keys for recaptcha, depending on status of ADDIKO_TEST
|
*/

define('ADDIKO_RECAPTCHA_LOCAL_SITE_KEY', '6LduBrsUAAAAAPcZcNqWRN86FFYKmm0RGQA9enDB');
define('ADDIKO_RECAPTCHA_LOCAL_SECRETE_KEY', '6LduBrsUAAAAAA3wzS5yDpH6tb5JaCQQc2dz36Hg'); // Recaptcha secret key
define('ADDIKO_CAPTCHA_LOCAL_CONFIDENCE', 0.5); // Recaptcha confidence

define('ADDIKO_RECAPTCHA_TEST_SITE_KEY', '6LfkptEUAAAAAKRxMlGTeyOeTHjENKIL6g4CKsDy');
define('ADDIKO_RECAPTCHA_TEST_SECRETE_KEY', '6LfkptEUAAAAAEgHW4eSFUT3xBKNVCXBm9YzNt4e'); // Recaptcha secret key
define('ADDIKO_CAPTCHA_TEST_CONFIDENCE', 0.5); // Recaptcha confidence

define('ADDIKO_RECAPTCHA_PRODUCTION_SITE_KEY', '6LfkptEUAAAAAKRxMlGTeyOeTHjENKIL6g4CKsDy'); // Recaptcha site key
define('ADDIKO_RECAPTCHA_PRODUCTION_SECRET_KEY', '6LfkptEUAAAAAEgHW4eSFUT3xBKNVCXBm9YzNt4e'); // Recaptcha secret key
define('ADDIKO_CAPTCHA_PRODUCTION_CONFIDENCE', 0.5); // Recaptcha confidence

defined('ADDIKO_ENKEY') OR define('ADDIKO_ENKEY', '0299807246445789az44'); // Addiko encrypt/decrypt key
defined('ADDIKO_MAILER') OR define('ADDIKO_MAILER', 'vas.savetnik.si@addiko.com'); // Addiko mailer


/*
|--------------------------------------------------------------------------
| Session UUID names
|--------------------------------------------------------------------------|
*/

defined('ADDIKO_TEMP_USER_UUID') OR define('ADDIKO_TEMP_USER_UUID', 'absi_temp_UUID'); // Addiko quiz user UUID session variable
defined('ADDIKO_USER_UUID') OR define('ADDIKO_USER_UUID', 'absi_UUID'); // Addiko quiz user UUID session variable


/*
|--------------------------------------------------------------------------
| Infobip API Credentials
|--------------------------------------------------------------------------
|
| Used to integrate Infobip API with Application.
|
*/
defined('ADDIKO_SMS_USERNAME')  OR define('ADDIKO_SMS_USERNAME',  'AddikoMNEstefan');
defined('ADDIKO_SMS_PASSWORD') OR define('ADDIKO_SMS_PASSWORD', '5oywB9fngHXicO9g');
defined('ADDIKO_SMS_SENDER') OR define('ADDIKO_SMS_SENDER', 'Addiko');
// defined('ADDIKO_SMS_USERNAME')  OR define('ADDIKO_SMS_USERNAME',  'AddikoM1');
// defined('ADDIKO_SMS_PASSWORD') OR define('ADDIKO_SMS_PASSWORD', '5XpqMYSFHSwQUs8Rk3Zm6FEfaKhyzzcn');
// defined('ADDIKO_SMS_SENDER') OR define('ADDIKO_SMS_SENDER', 'ADDIKO');


/*
|--------------------------------------------------------------------------
| Mautic API Credentials
|--------------------------------------------------------------------------
|
| Used to integrate Mautic and Mautic API with Application.
|
*/
defined('MAUTIC_URL')  OR define('MAUTIC_URL',  'https://abm-mautic.mar.addiko.com'); // Mautic URL (without ending /)
defined('MAUTIC_USER') OR define('MAUTIC_USER', 'ugStj9NZVsB4BArd'); // Mautic user
defined('MAUTIC_USER') OR define('MAUTIC_USER_ID', 1); // Mautic user
defined('MAUTIC_PASS') OR define('MAUTIC_PASS', '"JMghR@Y4QbPa=rr~;Z:VN579\kQ;b<~'); // Mautic user password


/*
|--------------------------------------------------------------------------
| Mautic FIELDS
|--------------------------------------------------------------------------
|
| Used to integrate Mautic and Mautic API with Application.
|
*/
define('MAUTIC_UNSUBSCRIBE_FIELD', 'channel_lmc');
define('MAUTIC_UNSUBSCRIBE_FIELD_VALUE', 'no');


/*
|--------------------------------------------------------------------------
| Dynamics Settings
|--------------------------------------------------------------------------
|
| Used to integrate Dynamics with Application.
|
*/
define('DYNAMICS_TEST_SERVER_URL',  'https://test-addiko.crm4.dynamics.com');
define('DYNAMICS_TEST_SERVER_USERNAME', 'test@haab.onmicrosoft.com');
define('DYNAMICS_TEST_SERVER_PASSWORD', 'AddikoCRM2019.');

define('DYNAMICS_PRODUCTION_SERVER_URL', 'https://haab.crm4.dynamics.com');
define('DYNAMICS_PRODUCTION_SERVER_USERNAME', 'test@haab.onmicrosoft.com');
define('DYNAMICS_PRODUCTION_SERVER_PASSWORD', 'AddikoCRM2019.');


define('DYNAMICS_TEST_CONTACT_SOURCE', '100000005');

define('DYNAMICS_TEST_NAMEN_SOGLASJA', '');
define('DYNAMICS_TEST_MARKETING_CHANNEL', '1B290EC4-BF65-E911-A840-000D3AB2DAFF');
define('DYNAMICS_TEST_MARKETING_SOURCE', '3DEFB047-C065-E911-A840-000D3AB2DAFF');
define('DYNAMICS_TEST_SOURCE_CAMPAIGN', '783BFB6B-C065-E911-A840-000D3AB2DAFF');

define('DYNAMICS_PRODUCTION_CONTACT_SOURCE', '100000005');

define('DYNAMICS_PRODUCTION_NAMEN_SOGLASJA', 'E908AE52-193F-E911-A855-000D3AB4FCE4');
define('DYNAMICS_PRODUCTION_MARKETING_CHANNEL', '96D9EBED-F6EC-E811-A846-000D3AB4FCE4');
define('DYNAMICS_PRODUCTION_MARKETING_SOURCE', '21237401-F7EC-E811-A846-000D3AB4FCE4');
define('DYNAMICS_PRODUCTION_SOURCE_CAMPAIGN', 'DFC03779-C777-E911-A850-000D3AB4FFEF');


/*
|--------------------------------------------------------------------------
| Dynamics Consent Fields
|--------------------------------------------------------------------------
|
| Used to integrate Dynamics with Application.
|
*/

define("DYNAMICS_GENERAL_CONSENT_EMAIL", 2);
define("DYNAMICS_GENERAL_CONSENT_PHONE", 3);
define("DYNAMICS_GENERAL_CONSENT_SMS", 4);

define("DYNAMICS_PRIZE_GAME_CONSENT_EMAIL", 19);
define("DYNAMICS_PRIZE_GAME_CONSENT_PHONE", 20);
define("DYNAMICS_PRIZE_GAME_CONSENT_SMS", 21);

define("DYNAMICS_BRIT_AWARDS_CONSENT_EMAIL", 22);
define("DYNAMICS_BRIT_AWARDS_CONSENT_PHONE", 23);
define("DYNAMICS_BRIT_AWARDS_CONSENT_SMS", 24);



/*
|--------------------------------------------------------------------------
| LMC routes
|--------------------------------------------------------------------------
|
| Used to integrate Mautic and Mautic API with Application.
|
*/

define('ADDIKO_ROUTE_REGISTRATION', 'registracija');
define('ADDIKO_ROUTE_VALIDATION', 'validacija');
define('ADDIKO_ROUTE_INFO', 'info');
define('ADDIKO_ROUTE_REGARDS', 'hvala');
define('ADDIKO_ROUTE_SESSION_EXPIRED', 'session_expired');