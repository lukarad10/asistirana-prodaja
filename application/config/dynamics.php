<?php
/**
 * Copyright (c) 2016 AlexaCRM.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// return [
//     // Dynamics CRM URL
//     'serverUrl'       => 'https://nlefortesting.crm4.dynamics.com',
//     // System user name
//     'username'        => 'demo@nlefortesting.onmicrosoft.com',
//     // System user password
//     'password'        => 'AddikoNLE2019.',
//     // CRM type (OnlineFederation for CRM Online, Federation for IFD)
//     'authMode'        => 'OnlineFederation',
//     'ignoreSslErrors' => true,
// ];
//return [
//	'serverUrl' => 'https://haab.crm4.dynamics.com', // Dynamics CRM URL
//	'username' => 'test@haab.onmicrosoft.com', // System user name
//	'password' => '1994Bojan1994', // 19Bojan94 // System user password
//	'authMode' => 'OnlineFederation', // CRM type (OnlineFederation for CRM Online, Federation for IFD)
//	'ignoreSslErrors' => true,
//];
#21.08.2019
return [
    'serverUrl' => DYNAMICS_SERVER_URL, // Dynamics CRM URL
    'username' => DYNAMICS_USERNAME, // System user name
    'password' => DYNAMICS_PASSWORD, // 19Bojan94 // System user password
    'authMode' => 'OnlineFederation', // CRM type (OnlineFederation for CRM Online, Federation for IFD)
    'ignoreSslErrors' => true,
];
