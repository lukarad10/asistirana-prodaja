<?php
/*
|--------------------------------------------------------------------------
| Environment settings
|--------------------------------------------------------------------------
|
| Settings for database and host
|
*/

define('ADDIKO_SESSION', md5('abs')); // Session name

if(ADDIKO_APPLICATION_STATUS == ADDIKO_APP_STATUS_LOCAL) {
    /*
    |--------------------------------------------------------------------------
    | Localhost database settings
    |--------------------------------------------------------------------------
    */

    define('ADDIKO_DB_HOST', '127.0.0.1:3306'); // DB host
    define('ADDIKO_DB_NAME', 'abm_asistirana_prodaja'); // DB name
    define('ADDIKO_DB_USER', 'root'); // DB user
    define('ADDIKO_DB_PASS', 'Koherencija21'); // DB user password

    define('ADDIKO_BASE_URL', 'http://localhost/asistirana-prodaja'); // Base url

    define('ADDIKO_COOKIE_SECURE', false); // Secure cookie

    define("ADDIKO_TEST", true);
    define("ADDIKO_SEND_SMS", false);
    define("ADDIKO_SEND_MAIL_INSTEAD_SMS", false);
    define("ADDIKO_SEND_THANK_YOU_MAIL", false);
    define("ADDIKO_CONNECT_DYNAMICS", false);
    define("ADDIKO_CONNECT_MAUTIC", false);

    define('ADDIKO_RECAPTCHA_SITE_KEY', ADDIKO_RECAPTCHA_LOCAL_SITE_KEY); // Recaptcha site key
    define('ADDIKO_RECAPTCHA_SECRET_KEY', ADDIKO_RECAPTCHA_LOCAL_SECRETE_KEY); // Recaptcha secret key
    define('ADDIKO_CAPTCHA_CONFIDENCE', ADDIKO_CAPTCHA_LOCAL_CONFIDENCE); // Recaptcha confidence

    define('DYNAMICS_SERVER_URL', DYNAMICS_TEST_SERVER_URL);
    define('DYNAMICS_USERNAME', DYNAMICS_TEST_SERVER_USERNAME);
    define('DYNAMICS_PASSWORD', DYNAMICS_TEST_SERVER_PASSWORD);

    define('DYNAMICS_CONTACT_SOURCE', DYNAMICS_TEST_CONTACT_SOURCE);
    define('DYNAMICS_NAMEN_SOGLASJA', DYNAMICS_TEST_NAMEN_SOGLASJA);
    define('DYNAMICS_MARKETING_CHANNEL', DYNAMICS_TEST_MARKETING_CHANNEL);
    define('DYNAMICS_MARKETING_SOURCE', DYNAMICS_TEST_MARKETING_SOURCE);
    define('DYNAMICS_SOURCE_CAMPAIGN', DYNAMICS_TEST_SOURCE_CAMPAIGN);


} else if(ADDIKO_APPLICATION_STATUS == ADDIKO_APP_STATUS_TEST) {
    /*
    |--------------------------------------------------------------------------
    | Test database settings0
    |--------------------------------------------------------------------------
    */

    define('ADDIKO_DB_HOST', 'localhost:3306'); // DB host
    define('ADDIKO_DB_NAME', 'abs_brit_awards'); // DB name
    define('ADDIKO_DB_USER', 'root'); // DB user
    define('ADDIKO_DB_PASS', 'kapone-011:BG'); // DB user password

    define('ADDIKO_BASE_URL', 'https://belimedved.m1.rs/addiko-asistirana-crna-gora/'); // Base url

    define('ADDIKO_COOKIE_SECURE', true); // Secure cookie

    define("ADDIKO_TEST", true);
    define("ADDIKO_SEND_SMS", false);
    define("ADDIKO_SEND_MAIL_INSTEAD_SMS", true);
    define("ADDIKO_SEND_THANK_YOU_MAIL", true);
    define("ADDIKO_CONNECT_DYNAMICS", false);
    define("ADDIKO_CONNECT_MAUTIC", false);

    define('ADDIKO_RECAPTCHA_SITE_KEY', ADDIKO_RECAPTCHA_TEST_SITE_KEY); // Recaptcha site key
    define('ADDIKO_RECAPTCHA_SECRET_KEY', ADDIKO_RECAPTCHA_TEST_SECRETE_KEY); // Recaptcha secret key
    define('ADDIKO_CAPTCHA_CONFIDENCE', ADDIKO_CAPTCHA_TEST_CONFIDENCE); // Recaptcha confidence

    define('DYNAMICS_SERVER_URL', DYNAMICS_TEST_SERVER_URL);
    define('DYNAMICS_USERNAME', DYNAMICS_TEST_SERVER_USERNAME);
    define('DYNAMICS_PASSWORD', DYNAMICS_TEST_SERVER_PASSWORD);

    define('DYNAMICS_CONTACT_SOURCE', DYNAMICS_TEST_CONTACT_SOURCE);
    define('DYNAMICS_NAMEN_SOGLASJA', DYNAMICS_TEST_NAMEN_SOGLASJA);
    define('DYNAMICS_MARKETING_CHANNEL', DYNAMICS_TEST_MARKETING_CHANNEL);
    define('DYNAMICS_MARKETING_SOURCE', DYNAMICS_TEST_MARKETING_SOURCE);
    define('DYNAMICS_SOURCE_CAMPAIGN', DYNAMICS_TEST_SOURCE_CAMPAIGN);


} else if(ADDIKO_APPLICATION_STATUS == ADDIKO_APP_STATUS_PRODUCTION) {
    /*
    |--------------------------------------------------------------------------
    | Production settings
    |--------------------------------------------------------------------------
    */

    define('ADDIKO_DB_HOST', '91.195.38.32:3306'); // DB host
    define('ADDIKO_DB_NAME', 'abs_brit_awards'); // DB name
    define('ADDIKO_DB_USER', 'abs_brit_awards'); // DB user
    define('ADDIKO_DB_PASS', 'Leesham4'); // DB user password

    define('ADDIKO_BASE_URL', 'https://www.addiko.si/glasbena-nagradna-igra'); // Base url

    define('ADDIKO_COOKIE_SECURE', true); // Secure cookie

    define("ADDIKO_TEST", false);
    define("ADDIKO_SEND_SMS", true);
    define("ADDIKO_SEND_MAIL_INSTEAD_SMS", false);
    define("ADDIKO_SEND_THANK_YOU_MAIL", true);
    define("ADDIKO_CONNECT_DYNAMICS", false);
    define("ADDIKO_CONNECT_MAUTIC", false);

    define('ADDIKO_RECAPTCHA_SITE_KEY', ADDIKO_RECAPTCHA_PRODUCTION_SITE_KEY); // Recaptcha site key
    define('ADDIKO_RECAPTCHA_SECRET_KEY', ADDIKO_RECAPTCHA_PRODUCTION_SECRET_KEY); // Recaptcha secret key
    define('ADDIKO_CAPTCHA_CONFIDENCE', ADDIKO_CAPTCHA_PRODUCTION_CONFIDENCE); // Recaptcha confidence

    define('DYNAMICS_SERVER_URL', DYNAMICS_PRODUCTION_SERVER_URL);
    define('DYNAMICS_USERNAME', DYNAMICS_PRODUCTION_SERVER_USERNAME);
    define('DYNAMICS_PASSWORD', DYNAMICS_PRODUCTION_SERVER_PASSWORD);

    define('DYNAMICS_CONTACT_SOURCE', DYNAMICS_PRODUCTION_CONTACT_SOURCE);
    define('DYNAMICS_NAMEN_SOGLASJA', DYNAMICS_PRODUCTION_NAMEN_SOGLASJA);
    define('DYNAMICS_MARKETING_CHANNEL', DYNAMICS_PRODUCTION_MARKETING_CHANNEL);
    define('DYNAMICS_MARKETING_SOURCE', DYNAMICS_PRODUCTION_MARKETING_SOURCE);
    define('DYNAMICS_SOURCE_CAMPAIGN', DYNAMICS_PRODUCTION_SOURCE_CAMPAIGN);
}