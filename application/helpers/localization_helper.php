<?php
/**
 * Created by PhpStorm.
 * User: Micha
 * Date: 3/20/2016
 * Time: 2:07 PM
 */

//function localize_url($url) {
//    $CI =& get_instance();
//    $locales = $CI->config->item("locales");
//    return base_url($_SESSION["lang"]."/".$_SESSION["city"]."/".$locales[$url][$_SESSION["lang"]]);
//}

function localize_date($date, $format) {
    $d = new DateTime($date);
    return $d->format($format);
}

function month_en_to_sr($date) {
    $en = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    $rs = ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"];

    return str_replace($en, $rs, $date);
}

function month_en_to_sr_short($date) {
    $en = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    $rs = ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec"];

    return str_replace($en, $rs, $date);
}

function eu_news_localize_date($date) {
    $lang = $_SESSION["lang"];

    if($lang == 'en') {
        $format = "F j, Y";
    } else {
        $format = "j. F Y.";
    }

    $d = new DateTime($date);
    $d = $d->format($format);

    if($lang == "rs"){
        return month_en_to_sr($d);
    } else {
        return $d;
    }
}

function eu_news_localize_datetime($date) {
    $lang = $_SESSION["lang"];

    if($lang == 'en') {
        $format = "F j, Y, H:i";
    } else {
        $format = "j. F Y. H:i";
    }

    $d = new DateTime($date);
    $d = $d->format($format);

    if($lang == "rs"){
        return month_en_to_sr($d);
    } else {
        return $d;
    }
}

function eu_project_localize_interval($start, $end, $no_end_time, $no_interval) {
    $lang = $_SESSION["lang"];

    if($no_end_time) {
        return eu_project_localize_date($start);
    }

    if($no_interval) {
        return "";
    }

    return eu_project_localize_date($start) . " - " . eu_project_localize_date($end);

}

function eu_project_localize_date($date) {
    $lang = $_SESSION["lang"];

    if($lang == "en") {
//        setlocale(LC_TIME, 'en_UK');
        $format = "F Y";
    } else {
//        setlocale(LC_TIME, 'sr_CS');
        $format = "F Y.";
    }

    $d = new DateTime($date);
    $d = $d->format($format);

    if($lang == "rs"){
        return month_en_to_sr($d);
    } else {
        return $d;
    }
}

function eu_project_localize_date_short($date) {
    $lang = $_SESSION["lang"];

    if($lang == "en") {
//        setlocale(LC_TIME, 'en_UK');
        $format = "M Y";
    } else {
//        setlocale(LC_TIME, 'sr_CS');
        $format = "M Y.";
    }

    $d = new DateTime($date);
    $d = $d->format($format);

    if($lang == "rs"){
        return month_en_to_sr_short($d);
    } else {
        return $d;
    }
}

function localize_url($url, $lang=null) {
    $original_lang = $lang;

    $CI =& get_instance();
    $locales = $CI->config->item("locales");
    $local_route = null;

    if(!$lang) $lang = $_SESSION["lang"];

    foreach($locales as $route => $definition) {
        if(preg_match('#^'.$route.'$#', $url, $matches)) {
            $local_route = $definition[$lang];

            foreach($matches as $key => $value) {
                if($key === "slug") {
                    if($original_lang) {
                        $CI->load->model('locales_model');
                        $new_slug = $CI->locales_model->get_locale($lang, $value);

                        if($new_slug) {
                            $local_route = str_replace("[".$key."]", $new_slug, $local_route);
                        } else {
                            $local_route = "";
                        }
                    } else {
                        $local_route = str_replace("[".$key."]", $value, $local_route);
                    }
                } else if($key === "sector") {
                    if($original_lang) {
                        $CI->load->model('locales_model');
                        $new_slug = $CI->locales_model->get_sector_locale($lang, $value);
                        if($new_slug) {
                            $local_route = str_replace("[".$key."]", $new_slug, $local_route);
                        } else {
                            $local_route = "";
                        }
                    } else {
                        $local_route = str_replace("[".$key."]", $value, $local_route);
                    }
                } else if($key === "search") {
                    $local_route = str_replace("[".$key."]", $value, $local_route);
                }

                else {
                    $local_route = str_replace("[".$key."]", $value, $local_route);
                }
            }

            if($local_route == "") {
                $local_route = $lang ."/";
            } else {
                if($local_route != null) $local_route = $lang ."/" . $local_route;
            }

            break;
        }
    }

    return base_url($local_route);
}

function localize_currency($number) {
    $lang = $_SESSION["lang"];
    if($lang == "rs") {
        $dec_point = ",";
        $step = ".";
    } else {
        $dec_point = ".";
        $step = ",";
    }

    return number_format($number, 0, $dec_point, $step);
}

function eu_format_interval_for_events($start_time, $end_time) {
    $start_dt = new DateTime($start_time);
    $end_dt = new DateTime($end_time);

    $ret = $start_dt->format("H:i") . " - ";

    if($start_dt->format("Y-m-d") < $end_dt->format("Y-m-d")) {
        $ret .= eu_news_localize_datetime($end_time);
    } else {
        $ret .= $end_dt->format("H:i");
    }

    return $ret;
}

function eu_format_interval_for_single_event($start_time, $end_time) {
    $start_dt = new DateTime($start_time);
    $end_dt = new DateTime($end_time);

    $ret = $start_dt->format("H:i") . " - ";

    if($start_dt->format("Y-m-d") < $end_dt->format("Y-m-d")) {
        $ret = eu_news_localize_date($start_time) . " - " . eu_news_localize_date($end_time);
    } else {
        $ret = eu_news_localize_date($start_time);
    }

    return $ret;
}

function eu_format_interval_time_for_single_event($start_time, $end_time) {
    $start_dt = new DateTime($start_time);
    $end_dt = new DateTime($end_time);

    if($start_dt->format("Y-m-d") == $end_dt->format("Y-m-d")) {
        $ret = $start_dt->format("H:i") . " - " . $end_dt->format("H:i");
    } else {
        $ret = $start_dt->format("H:i");
    }

    return $ret;
}

function eu_format_interval_for_tags_page($start_time, $end_time) {
    $start_dt = new DateTime($start_time);
    $end_dt = new DateTime($end_time);

    $ret = eu_news_localize_date($start_time) . " " . $start_dt->format("H:i") . " - ";

    if($start_dt->format("Y-m-d") < $end_dt->format("Y-m-d")) {
        $ret .= eu_news_localize_date($end_time) . " " . $end_dt->format("H:i");
    } else {
        $ret .= $end_dt->format("H:i");
    }

    return $ret;
}