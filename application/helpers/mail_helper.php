<?php

function send_mail($from_name, $from_mail, $mail_subject, $mail_message) {
    $to = "cirkuzant@gmail.com";
    $headers = "From:" . $from_mail;
    $mail_sent = mail($to, $mail_subject, $mail_message, $headers);
    return $mail_sent;
}

function send_html_mail($from_name, $from_mail, $mail_subject, $mail_message, $to = "cirkuzant@gmail.com") {
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    $headers .= "From:" . $from_mail;
    $mail_sent = mail($to, $mail_subject, $mail_message, $headers);
    return $mail_sent;
}

function send_mail_with_attachment($from_name, $from_mail, $mail_subject, $mail_message, $to = "cirkuzant@gmail.com", $file){
//    $file = $path.$filename;
    $content = file_get_contents( $file);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));
    $name = rawurldecode(basename($file));


// header
    $header = "From: ".$from_name." <".$from_mail.">\r\n";
    $header .= "Reply-To: ".$from_mail."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";

// message & attachment
    $nmessage = "--".$uid."\r\n";
    $nmessage .= "Content-type:text/html; charset=UTF-8\r\n";
    $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $nmessage .= $mail_message."\r\n\r\n";
    $nmessage .= "--".$uid."\r\n";
    $nmessage .= "Content-Type: application/octet-stream; name=\"".$name."\"\r\n";
    $nmessage .= "Content-Transfer-Encoding: base64\r\n";
    $nmessage .= "Content-Disposition: attachment; filename=\"".$name."\"\r\n\r\n";
    $nmessage .= $content."\r\n\r\n";
    $nmessage .= "--".$uid."--";

    $mail_sent = mail($to, $mail_subject, $nmessage, $header);
    return $mail_sent;
}
