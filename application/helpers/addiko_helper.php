<?php

// GORAN: new uuid function
function generate_uuid() {
    return uniqid(time() . "-", true);
}

function format_mobile_number($mobile) {
    if (substr($mobile, 0, 2) == '00') {
        $mobile = substr($mobile, 2);
    } else if (substr($mobile, 0, 1) == '+') {
        $mobile = substr($mobile, 1);
    } else {
        $mobile = '382' . substr($mobile, 1);
    }
    $mobile = preg_replace('/\D/', '', $mobile);

    return $mobile;
}

function generate_otp()
{
    return sprintf('%d', mt_rand(100000, 999999));
}

/* ****************************************************
OLD FUNCTIONS
******************************************************/

if (!function_exists('addiko_crypt')) {
	function addiko_crypt($string)
	{
		$string = base64_encode($string);

		$string = str_replace('=', '', $string);

		$string = strrev($string);

		$string = str_rot13($string);

		return $string;
	} // end of function
} // end if

if (!function_exists('addiko_decrypt')) {
	function addiko_decrypt($string)
	{
		$string = str_rot13($string);

		$string = strrev($string);

		$string = str_replace('=', '', $string);

		$string = base64_decode($string);

		return $string;
	} // end of function
} // end if

if (!function_exists('addiko_db_encrypt')) {
	function addiko_db_encrypt($string, $key)
	{
	    $random = rand(1000, 9999);
	    $key = hash('ripemd160', '%s%' . $key);
	    $string = bin2hex($string);
	    $time = time();
	    $string = $time . $key . $string;
	    $string = str_rot13($string);
	    $string = $random . $string;
	    $string = base64_encode($string);
	    $string = str_replace('=', '', $string);
		return $string;
	} // end of function
} // end if

if (!function_exists('addiko_db_decrypt')) {
	function addiko_db_decrypt($string, $key)
	{
	    $key = hash('ripemd160', '%s%' . $key);
	    $string = base64_decode($string);
	    $string = substr($string, 4);
	    $string = str_rot13($string);
	    $string = explode($key, $string);
	    $time = array_shift($string);
	    $string = implode('', $string);
	    $string = hex2bin($string);
		return $string;
	} // end of function
} // end if
