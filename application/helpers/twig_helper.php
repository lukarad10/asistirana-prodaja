<?php
/**
 * Created by PhpStorm.
 * User: Micha
 * Date: 2/7/2016
 * Time: 2:17 PM
 */

function public_url($url=null) {
    return base_url("public/" . $url);
}

function run_module($module) {
    $args = func_get_args();
    $output = call_user_func_array(["Modules", "run"], $args);
    return $output;
}

function excerpt($text, $length) {
    return strip_tags(word_limiter($text, $length));
}

function str_pos($haystack, $needle) {
    $res = strpos($haystack, $needle);

    if($res === false) $res = -1;

    return $res;
}

function format_only_date($d) {
    return date('j M, Y', strtotime($d));
}

function format_date($d) {
    return date('l, M j, Y, G:i:s', strtotime($d));
}

function format_date_short($d) {
    return date('M j, Y, G:i:s', strtotime($d));
}

function format_time($d) {
    return date('G:i:s', strtotime($d));
}

function format_extensions($ext) {
    $ret = str_replace(',', ', ', $ext);

    $pos = strrpos($ret, ', ');

    if($pos !== false) {
        $ret = substr_replace($ret, ' or ', $pos, 2);
    }

    return $ret;
}

function check_value($val, $str) {
    return in_array($val, explode(',', $str));
}

function check_valuelist_value($line, $json_from_table) {
    if($json_from_table) {
        $values = json_decode($json_from_table, true);

        for($i=0; $i<count($values); $i++) {
            if($line == $values[$i]['id']) return true;
        }
    }

    return false;
}

function find_valuelist_value($line, $json_from_table) {
    if($json_from_table) {
        $values = json_decode($json_from_table, true);

        for($i=0; $i<count($values); $i++) {
            if($line == $values[$i]['id']) return $values[$i]['value'];
        }
    }

    return false;
}

function get_extension($name) {
    return pathinfo($name, PATHINFO_EXTENSION);
}

function find_in_array($arr, $field_name, $value) {
    for($i=0; $i<count($arr); $i++) {
        if($arr[$i][$field_name] == $value) return $arr[$i];
    }

    return null;
}

function format_currency($number) {
    return number_format($number, 0, '.', ',');
}

//function localize_date($date) {
//    //TODO this function has to be rwritten but for now it will display only one format
//
//    return date('M j, Y G:i:s', strtotime($date));
//}


function dashboard_most_visited_vendors_percent($total, $total_month) {
    if($total_month == 0) {
        $percent = -100;
    } else {
        try {
            @$percent = round(((($total / ($total_month /30)) - 1) * 100), 2);
        } catch (Exception $e) {
            $percent = 0;
        }
    }

    return $percent;
}

function remove_serbian_letter($string){
    $string = str_replace("š", "s", $string);
    $string = str_replace("đ", "d", $string);
    $string = str_replace("ž", "z", $string);
    $string = str_replace("č", "c", $string);
    $string = str_replace("ć", "c", $string);

    return $string;
}

function replace_space_with_dash($string){
    $string = str_replace(" ", "-", $string);

    return $string;
}

function latitude($coord) {
    $arr = explode(',', $coord);

    return $arr[0];
}

function longitude($coord) {
    $arr = explode(',', $coord);

    return $arr[1];
}

function transform_to_thumbnail($url) {
    return strrev(implode(strrev("/thumbnail/"), explode(strrev("/"), strrev($url), 2)));
}

function create_paging($url, $page, $number_of_pages) {
    $arr = [];
    if($number_of_pages > 1) {
        $arr[] = 1;
        if($page - 1 > 1) $arr[] = $page - 1;
        if($page > 1 && $page != $number_of_pages) $arr[] = $page;
        if($page + 1 < $number_of_pages) $arr[] = $page + 1;
        $arr[] = $number_of_pages;

        $html = '<section class="pagination_container"><ul class="pagination_list">';

        for($i=0; $i<count($arr); $i++) {
            if($i > 0) {
                if($arr[$i] - $arr[$i-1] > 1) $html .= '<li class="three-points">&nbsp;...&nbsp;</li>';
            }
            $html .= '<li class="pagination_list_item';
            if($arr[$i] == $page) $html .= ' active';

            $html .= '"><a href="' . $url . '/' . $arr[$i] . '" class="pagination_list_link">' . $arr[$i] . '</a></li>';
        }

        $html .= '</ul></section>';

        return $html;
    }
}

function handle_sectors($sectors, $article, $lang) {
    $project_sectors = explode(",", $article["sector"]);
    $tmp = [];

    for($j=0; $j<count($project_sectors); $j++) {
        for($k=0; $k<count($sectors); $k++) {
            if(intval($project_sectors[$j]) == $sectors[$k]["id"]) {
                $tmp[] = [
                    "slug" => $sectors[$k]["slug_" . $lang],
                    "title" => $sectors[$k]["name_" . $lang]
                ];
            }
        }
    }

    $article["sectors"] = $tmp;
    return $article;
}

function handle_sectors_for_array($sectors, $articles, $lang) {
    $ret = [];

    for($i=0; $i<count($articles); $i++) {
        $ret[] = handle_sectors($sectors, $articles[$i], $lang);
    }

    return $ret;
}