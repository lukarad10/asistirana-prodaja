<?php




function dynamics_get_contact($dynamics, $email) {
    $contact = $dynamics->getContactByEmail($email);

    if ($contact->Entities != null) {
        return $contact->Entities[0];
    } else {
        return null;
    }
}

function dynamics_unsubsribe($email, $consent_type) {
    $dynamics = new Dynamics();

    $contact = dynamics_get_contact($dynamics, $email);

    if($contact) {
        $date_of_validity = strtotime('31.12.2019');
        $dynamics->updateConsent($contact, $consent_type, $date_of_validity, "0");
        return true;
    } else {
        return false;
    }
}


/**
 * @param $lead
 */
function dynamics_insert_lead($lead) {
    $general_consent_date_of_validity = strtotime('23.05.2120');
    $prize_game_date_of_validity = strtotime('31.12.2019');

    $dynamics = new Dynamics();
    $getContactByEmail = $dynamics->getContactByEmail($lead['email']);

    if ($getContactByEmail->Entities == null) {
        $contact = $dynamics->createContact($lead['firstname'], $lead['lastname'], $lead['email'], $lead['mobile']);
        $getContactByEmail = $dynamics->getContactByEmail($lead['email']);
    }

    $contact = $getContactByEmail->Entities[0];
//    var_dump($contact);

    //General consents
    if ($lead['general_consent'] == 1) {
        $dynamics->updateConsent($contact, DYNAMICS_GENERAL_CONSENT_EMAIL, $general_consent_date_of_validity, "1");
        $dynamics->updateConsent($contact, DYNAMICS_GENERAL_CONSENT_PHONE, $general_consent_date_of_validity, "1");
        $dynamics->updateConsent($contact, DYNAMICS_GENERAL_CONSENT_SMS, $general_consent_date_of_validity, "1");
    }

    // Prize game consents
    $dynamics->updateConsent($contact, DYNAMICS_PRIZE_GAME_CONSENT_EMAIL, $prize_game_date_of_validity, "1");
    $dynamics->updateConsent($contact, DYNAMICS_PRIZE_GAME_CONSENT_PHONE, $prize_game_date_of_validity, "1");
    $dynamics->updateConsent($contact, DYNAMICS_PRIZE_GAME_CONSENT_SMS, $prize_game_date_of_validity, "1");

    // Brit Awards consents
    $dynamics->updateConsent($contact, DYNAMICS_BRIT_AWARDS_CONSENT_EMAIL, $prize_game_date_of_validity, "1");
    $dynamics->updateConsent($contact, DYNAMICS_BRIT_AWARDS_CONSENT_PHONE, $prize_game_date_of_validity, "1");
    $dynamics->updateConsent($contact, DYNAMICS_BRIT_AWARDS_CONSENT_SMS, $prize_game_date_of_validity, "1");
}


/**
 * @param $lead
 */
function dynamics_insert_lead_old($lead) {
    $general_consent_date_of_validity = strtotime('23.05.2120');
    $prize_game_date_of_validity = strtotime('31.12.2019');

    $dynamics = new Dynamics();
    $getContactByEmail = $dynamics->getContactByEmail($lead['email']);

    if ($getContactByEmail->Entities == null) {
        // Create contact
        $dynamics->createContact($lead['firstname'], $lead['lastname'], $lead['email'], $lead['mobile']);

        //General consents
        if ($lead['general_consent'] == 1) {
            $dynamics->createConsents(DYNAMICS_GENERAL_CONSENT_EMAIL, $general_consent_date_of_validity);
            $dynamics->createConsents(DYNAMICS_GENERAL_CONSENT_PHONE, $general_consent_date_of_validity);
            $dynamics->createConsents(DYNAMICS_GENERAL_CONSENT_SMS, $general_consent_date_of_validity);
        }

        // Prize game consents
        $dynamics->createConsents(DYNAMICS_PRIZE_GAME_CONSENT_EMAIL, $prize_game_date_of_validity);
        $dynamics->createConsents(DYNAMICS_PRIZE_GAME_CONSENT_PHONE, $prize_game_date_of_validity);
        $dynamics->createConsents(DYNAMICS_PRIZE_GAME_CONSENT_SMS, $prize_game_date_of_validity);

        // Brit Awards consents
//        $dynamics->createConsents(DYNAMICS_BRIT_AWARDS_CONSENT_EMAIL, $prize_game_date_of_validity);
//        $dynamics->createConsents(DYNAMICS_BRIT_AWARDS_CONSENT_PHONE, $prize_game_date_of_validity);
//        $dynamics->createConsents(DYNAMICS_BRIT_AWARDS_CONSENT_SMS, $prize_game_date_of_validity);
    } else {
        $entity = $getContactByEmail->Entities[0];

        //General consents
        if ($lead['general_consent'] == 1) {
            $dynamics->updateContact($entity,DYNAMICS_GENERAL_CONSENT_EMAIL, $general_consent_date_of_validity);
            $dynamics->updateContact($entity,DYNAMICS_GENERAL_CONSENT_PHONE, $general_consent_date_of_validity);
            $dynamics->updateContact($entity,DYNAMICS_GENERAL_CONSENT_SMS, $general_consent_date_of_validity);
        }

        // Prize game consents
        $dynamics->updateContact($entity,DYNAMICS_PRIZE_GAME_CONSENT_EMAIL, $prize_game_date_of_validity);
        $dynamics->updateContact($entity,DYNAMICS_PRIZE_GAME_CONSENT_PHONE, $prize_game_date_of_validity);
        $dynamics->updateContact($entity,DYNAMICS_PRIZE_GAME_CONSENT_SMS, $prize_game_date_of_validity);

        // Brit Awards consents
//        $dynamics->updateContact($entity,DYNAMICS_BRIT_AWARDS_CONSENT_EMAIL, $prize_game_date_of_validity);
//        $dynamics->updateContact($entity,DYNAMICS_BRIT_AWARDS_CONSENT_PHONE, $prize_game_date_of_validity);
//        $dynamics->updateContact($entity,DYNAMICS_BRIT_AWARDS_CONSENT_SMS, $prize_game_date_of_validity);
    }
}
