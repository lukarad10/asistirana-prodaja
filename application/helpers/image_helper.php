<?php

function resample_and_save_image($fit_mode, $field_name, $save_path, $target_width, $target_height) {
    $src = imagecreatefromfile($field_name);
    $ext = strtolower(pathinfo($save_path, PATHINFO_EXTENSION));

    list($width, $height) = getimagesize($_FILES[$field_name]['tmp_name']);
    list($src_x, $src_y, $final_width, $final_height, $src_width, $src_height) = get_dimensions($fit_mode, $width, $height, $target_width, $target_height);

    $new_img = imagecreatetruecolor($final_width, $final_height);

    if($ext == 'png') {
        imagealphablending($new_img, false);
        imagesavealpha($new_img,true);
        $transparent = imagecolorallocatealpha($new_img, 255, 255, 255, 127);
        imagefilledrectangle($new_img, 0, 0, $final_width, $final_height, $transparent);
    }

    imagecopyresampled($new_img, $src, 0, 0, $src_x, $src_y, $final_width, $final_height, $src_width, $src_height);

    if($ext == 'jpg') imagejpeg($new_img, $save_path, 80);
    elseif($ext == 'png') imagepng($new_img, $save_path, 0);
    elseif($ext == 'gif') imagegif($new_img, $save_path);

    imagedestroy($new_img);
    imagedestroy($src);
}

function resample_and_save_forum_image($fit_mode, $image_path, $save_path, $target_width, $target_height) {
//    $src = imagecreatefromfile($field_name);

    $ext = strtolower(pathinfo($save_path, PATHINFO_EXTENSION));

    switch(strtolower($ext)){
        case "jpg":
        case "jpeg":
            $src = imagecreatefromjpeg($image_path);
            break;
        case "png":
            $src = imagecreatefrompng($image_path);
            break;
        default:
            throw new InvalidArgumentException('File "'.$image_path.'" is not valid jpg, png or gif image.');
            break;
    }

    list($width, $height) = getimagesize($image_path);
    list($src_x, $src_y, $final_width, $final_height, $src_width, $src_height) = get_dimensions($fit_mode, $width, $height, $target_width, $target_height);

    $new_img = imagecreatetruecolor($final_width, $final_height);

    if($ext == 'png') {
        imagealphablending($new_img, false);
        imagesavealpha($new_img,true);
        $transparent = imagecolorallocatealpha($new_img, 255, 255, 255, 127);
        imagefilledrectangle($new_img, 0, 0, $final_width, $final_height, $transparent);
    }

    imagecopyresampled($new_img, $src, 0, 0, $src_x, $src_y, $final_width, $final_height, $src_width, $src_height);

    if($ext == 'jpg') imagejpeg($new_img, $save_path, 80);
    elseif($ext == 'png') imagepng($new_img, $save_path, 0);
    elseif($ext == 'gif') imagegif($new_img, $save_path);

    imagedestroy($new_img);
    imagedestroy($src);
}

function resample_and_save_image_ivan() {
    $target_width = 400;
    $target_height = 400;
    $fit_mode = 'crop';

    foreach(glob('public/articles/image/*.[jJ][pP][gG]') as $image){
//    $image = 'public/articles/image/868.jpg';
        $image_name = explode("/", $image);
        $image_name = $image_name[count($image_name) - 1];

        $save_path = "public/articles/image/thumbnail_test/".$image_name;
        $src = imagecreatefromfileIvan($image, $image_name);
        $ext = strtolower(pathinfo($save_path, PATHINFO_EXTENSION));

        list($width, $height) = getimagesize($image);
        list($src_x, $src_y, $final_width, $final_height, $src_width, $src_height) = get_dimensions($fit_mode, $width, $height, $target_width, $target_height);

        $new_img = imagecreatetruecolor($final_width, $final_height);

        if($ext == 'png') {
            imagealphablending($new_img, false);
            imagesavealpha($new_img,true);
            $transparent = imagecolorallocatealpha($new_img, 255, 255, 255, 127);
            imagefilledrectangle($new_img, 0, 0, $final_width, $final_height, $transparent);
        }

        imagecopyresampled($new_img, $src, 0, 0, $src_x, $src_y, $final_width, $final_height, $src_width, $src_height);

        if($ext == 'jpg') imagejpeg($new_img, $save_path, 80);
        elseif($ext == 'png') imagepng($new_img, $save_path, 0);
        elseif($ext == 'gif') imagegif($new_img, $save_path);

        imagedestroy($new_img);
        imagedestroy($src);
    }

    echo("<span style='font-size: 100px;'>FINISHED</span>");
}

function get_dimensions($fit_mode, $width, $height, $target_width, $target_height) {
    $ratio = $width / $height;
    $target_ratio = $target_width / $target_height;

    $src_x = 0;
    $src_y = 0;

    if($fit_mode == "best-fit") {
        if($ratio > $target_ratio) {
            $final_width = $target_width;
            $final_height = round($target_width / $ratio);
        } else {
            $final_height = $target_height;
            $final_width = round($target_height * $ratio);
        }

    } elseif($fit_mode == 'fit-width') {
        $final_width = $target_width;
        $final_height = $target_width / $ratio;

    } elseif($fit_mode == 'fit-height') {
        $final_height = $target_height;
        $final_width = round($target_height * $ratio);

    } elseif($fit_mode == 'crop') {
        $final_width = $target_width;
        $final_height = $target_height;

        if($ratio > $target_ratio) {
            $src_x = round(($width - $height * $target_ratio) / 2);
            $src_y = 0;

            $width = round($height * $target_ratio);

        } else {
            $src_x = 0;
            $src_y = round(($height - $width / $target_ratio) / 2);

            $height = round($width / $target_ratio);
        }
    }
    return [$src_x, $src_y, $final_width, $final_height, $width, $height];
}

function imagecreatefromfile($field_name) {
    $tmp_name = $_FILES[$field_name]['tmp_name'];
    $file_name = $_FILES[$field_name]['name'];

    if(!file_exists($tmp_name)) {
        throw new InvalidArgumentException('File "'.$tmp_name.'" not found.');
    }

    switch (strtolower(pathinfo($file_name, PATHINFO_EXTENSION))) {
        case 'jpeg':
        case 'jpg':
            return imagecreatefromjpeg($tmp_name);
            break;

        case 'png':
            $new_image = imagecreatefrompng($tmp_name);
            imagesavealpha($new_image, true);
            return $new_image;
            break;

        case 'gif':
            return imagecreatefromgif($tmp_name);
            break;

        default:
            throw new InvalidArgumentException('File "'.$file_name.'" is not valid jpg, png or gif image.');
            break;
    }
}

function imagecreatefromfileIvan($image, $image_name) {
    $tmp_name = $image;
    $file_name = $image_name;

    if(!file_exists($tmp_name)) {
        throw new InvalidArgumentException('File "'.$tmp_name.'" not found.');
    }

    switch (strtolower(pathinfo($file_name, PATHINFO_EXTENSION))) {
        case 'jpeg':
        case 'jpg':
            return imagecreatefromjpeg($tmp_name);
            break;

        case 'png':
            $new_image = imagecreatefrompng($tmp_name);
            imagesavealpha($new_image, true);
            return $new_image;
            break;

        case 'gif':
            return imagecreatefromgif($tmp_name);
            break;

        default:
            throw new InvalidArgumentException('File "'.$file_name.'" is not valid jpg, png or gif image.');
            break;
    }
}

function get_accept($ext) {
    return str_replace(['png', 'jpg', 'gif'], ['image/png', 'image/jpeg', 'image/gif'], $ext);
}
