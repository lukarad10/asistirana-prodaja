<?php


require APPPATH."third_party/MX/Controller.php";

/**
 * @property database_model $database_model
 */
class CIR_Controller extends MX_Controller {

	public function __construct()
    {
        parent::__construct();

        date_default_timezone_set("Europe/Belgrade");

        $this->twig->module = str_replace(["../modules/", "controllers/"], ["", ""], $this->router->directory);

        $this->twig->addGlobal("MAUTIC_URL", MAUTIC_URL);

        $this->twig->addGlobal("ADDIKO_TEST", ADDIKO_TEST);
        $this->twig->addGlobal("ADDIKO_RECAPTCHA_SITE_KEY", ADDIKO_RECAPTCHA_SITE_KEY);
        $this->twig->addGlobal("recaptcha_error", 0);

        if (@$_SESSION['session_token'] == '') {
            $_SESSION['session_token'] = getToken(32);
        }

        $this->load->model("database_model");
    }
}


class CIR_MainController extends CIR_Controller {
    protected $current_position = ADDIKO_POSITION_NOT_SET;

    public function __construct()
    {
        parent::__construct();

//        $this->check_for_redirection();

//        $_SESSION["ADDIKO_POSITION"] = $this->current_position;
    }

    private function check_for_redirection()
    {
        if(!isset($_SESSION["ADDIKO_POSITION"])) {
            $session_position = -1;
        } else {
            $session_position = $_SESSION["ADDIKO_POSITION"];
        }

        if(!isset($_SESSION["ADDIKO_NEXT_POSITION"])) {
            $next_position = 0;
        } else {
            $next_position = $_SESSION["ADDIKO_NEXT_POSITION"];
        }

//        var_dump("Current position: " . $this->current_position);
//        var_dump("Session position: " . $session_position);
//        var_dump("Next position: " . $next_position);

        if($this->current_position < $session_position) {
            var_dump("SMALLER");
            redirect(base_url(ADDIKO_SESSION_REDIRECTIONS[$session_position]));
        }

        if($this->current_position > $next_position) {
            var_dump("LARGER");
            redirect(base_url(ADDIKO_SESSION_REDIRECTIONS[$session_position]));
        }
    }
}