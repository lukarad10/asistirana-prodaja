<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * @property CI_Config $config
 * @property CI_DB_mysql_driver $db
 * @property CI_DB_forge $dbforge
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Form_validation $form_validation
 * @property CI_FTP $ftp
 * @property CI_Input $input
 * @property CI_Loader $load
 * @property CI_Parser $parser
 * @property CI_Session $session
 * @property CI_Table $table
 * @property CI_URI $uri
 */

class CIR_Model extends CI_Model {
    public $table = '';
    public $lang;

    public function __construct() {
        parent::__construct();

        date_default_timezone_set("Europe/Ljubljana");
    }

    public function get_array($query, $input=null)
    {
        $run_query = $this->db->query($query, $input);
        $result = $run_query->result_array();
        $run_query->free_result();

        return $result;
    }

    public function get_row($query, $input)
    {
        $run_query = $this->db->query($query, $input);
        $result = $run_query->result_array();
        $run_query->free_result();

        if(count($result) > 0) return $result[0];
        else return null;
    }

    public function get_value($query, $field_name, $input=null)
    {
        $run_query = $this->db->query($query, $input);
        $result = $run_query->result_array();
        $run_query->free_result();

//        var_dump($result[0][$field_name]);

        if(count($result) > 0) {
            return $result[0][$field_name];
        } else {
            return null;
        }
    }

    public function insert_row($table, $fields, $values)
    {
        $f = implode(',', $fields);

        $v = '';
        $first = true;

        for($i=0; $i<count($values); $i++) {
            if($first) {
                $v .= '?';
                $first = false;
            } else {
                $v .= ', ?';
            }
        }

        $query = "INSERT INTO $table ($f) VALUES($v)";
        $this->db->query($query, $values);

        return $this->db->insert_id();
    }

    public function update_row($table, $id, $fields, $values)
    {
        $set = '';
        $first = true;

        for($i=0; $i<count($fields); $i++) {
            if($first) {
                $set .= ' SET ' . $fields[$i] . '=?';
                $first = false;
            } else {
                $set .= ', ' . $fields[$i] . '=?';
            }
        }

        $values[] = $id;

        $query = "UPDATE $table $set WHERE id=?";

        $this->db->query($query, $values);
    }

    public function delete_row($table, $id)
    {
        $query = "DELETE FROM $table WHERE id=?";

        $this->db->query($query, [$id]);
    }

    protected function insert_action($article_id, $user_id, $type_id, $item_type, $action, $description, $data=null) {
        $this->insert_row('action_log', ['article_id', 'user_id', 'type_id', 'item_type', 'action', 'description', 'data'],
            [$article_id, $user_id, $type_id, $item_type, $action, $description, $data]);
    }
}

/**
 * @property CI_Config $config
 * @property CI_DB_mysql_driver $db
 * @property CI_DB_forge $dbforge
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Form_validation $form_validation
 * @property CI_FTP $ftp
 * @property CI_Input $input
 * @property CI_Loader $load
 * @property CI_Parser $parser
 * @property CI_Session $session
 * @property CI_Table $table
 * @property CI_URI $uri
 */

class CIR_LangModel extends CIR_Model {
    protected function add_image_name_to_array($array, $mode, $original_field_name, $final_field_name)
    {
        if($mode == "array") {
            for($i=0; $i<count($array); $i++) {
                $array[$i] = $this->add_image_name($array[$i], $original_field_name, $final_field_name);
            }
        } else {
            $array = $this->add_image_name($array, $original_field_name, $final_field_name);
        }

        return $array;

    }

    protected function add_image_name($array, $original_field_name, $final_field_name)
    {
        if($array) {
            if($array[$original_field_name]) {
                $array[$final_field_name] = $array["id"] . "." . strtolower(pathinfo($array[$original_field_name], PATHINFO_EXTENSION));
            } else {
                $array[$final_field_name] = null;
            }

//            unset($array[$original_field_name]);
        }

        return $array;
    }

    protected function add_paging(&$query, &$query_data, $page, $page_size)
    {
        if(!is_null($page_size)) {
            $page_size = intval($page_size);
            $query .= ' LIMIT ?';
            $query_data[] = $page_size;

            if(!is_null($page)) {
                $page = intval($page);
                if($page <= 0) $page=1;

                $query .= " OFFSET ?";

                $start = ($page - 1) * $page_size;
                $query_data[] = intval($start);
            }
        }
    }

    protected function split_tags($arr) {
        for($i=0; $i<count($arr); $i++) {
            $arr[$i]["tags"] = explode(",", $arr[$i]["tags"]);
        }

        return $arr;
    }
}