<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!class_exists('Database_model')) {
	/**
	 * Database model
	 *
	 * Create, update and check leads.
	 *
	 * @package	CodeIgniter
	 * @subpackage AddikoQuiz
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	class Database_model extends CIR_Model {
		public function __construct()
		{
			parent::__construct();
		}

        /**
         * @param $firstname
         * @param $lastname
         * @param $jmbg
         * @param $email
         * @param $mobile
         * @param $expositure
         * @param $kreditni_biro_consent
         * @param $general_consent
         * @param $temp_uuid
         * @param $recaptcha_result
         */
        public function insert_temp_lead_and_send_otp(
		    $firstname,
            $lastname,
            $jmbg,
            $email,
            $mobile,
            $expositure,
            $kreditni_biro_consent,
            $general_consent,
            $temp_uuid,
            $recaptcha_result
        ) {
            $addiko_mailer = ADDIKO_MAILER;

            // Generate OTP
            $generated_otp = generate_otp();

            // Insets lead into table
            $this->insert_row("verify", [
                "firstname",
                "lastname",
                "jmbg",
                "email",
                "uuri",
                "mobile",
                "expositure",
                "kreditni_biro_consent",
                "general_consent",
                "temp_uuid",
                "otp",
                "recaptcha_confidence",
                "ip"
            ], [
                $firstname,
                $lastname,
                $jmbg,
                $email,
                html_escape(addiko_crypt($email)),
                $mobile,
                $expositure,
                $kreditni_biro_consent,
                $general_consent,
                $temp_uuid,
                $generated_otp,
                $recaptcha_result["score"],
                $_SERVER['REMOTE_ADDR']
            ]);

            if (ADDIKO_SEND_SMS) {
                $sms = new Sms();
                $sms->send_sms(html_escape($mobile), $generated_otp);
            }

            if (ADDIKO_SEND_MAIL_INSTEAD_SMS) {
                $email = html_escape($email);

                $headers[] = "From: {$addiko_mailer}";
                mail($email, 'Verifikacioni kod - Test mautic', "Vaš verifikacioni kod je {$generated_otp}", implode("\r\n", $headers));
            }
        }

        public function update_temp_lead($temp_uuid) {
            $this->db->query("UPDATE verify SET verified=1 WHERE temp_uuid=?", [$temp_uuid]);
        }

        public function check_if_phone_exists($phone) {
            return $this->get_row("SELECT * FROM leads WHERE mobile=?", [format_mobile_number($phone)]);
        }

        public function check_if_email_exists($email) {
            return $this->get_row("SELECT * FROM leads WHERE email=?", [$email]);
        }

        public function check_if_sms_code_valid($string) {
            if($this->get_row("SELECT * FROM verify WHERE temp_uuid=? AND otp=? ", [$_SESSION[ADDIKO_TEMP_USER_UUID], $string])) {
//                var_dump($this->db->last_query() . ': sms code is valid');
                return true;
            } else {
//                var_dump($this->db->last_query() . ': sms code is INVALID');
                return false;
            }
        }

        public function check_if_sms_code_expired($otp) {
            $now = date('Y-m-d H:i:s');

            if($this->get_row("SELECT * FROM verify WHERE temp_uuid=? AND created_at >= DATE_SUB(?, INTERVAL 5 MINUTE) AND otp=?",
                [
                    $_SESSION[ADDIKO_TEMP_USER_UUID],
                    $now,
                    $otp
                ]
            )) {
                return true;
            } else {
                return false;
            }
        }

        public function get_temp_lead_by_uuid($temp_uuid) {
            return $this->get_row("SELECT * FROM verify WHERE temp_uuid=?", [$temp_uuid]);
        }

        public function get_calc_by_uuid($uuid) {
            return $this->get_row("SELECT * FROM calculator WHERE uuid=?", [$uuid]);
        }


		/**
		 * Delete expired codes
		 *
		 * @version 1.0.0
		 * @since 1.0.0
		 */
		public function delete_expired_codes()
		{
			// Verify table
			$verify_table = ADDIKO_TABLES['verify']['name'];

			// Geting time (NOW!)
			$now = date('Y-m-d H:i:s');

			// Query that looking for code and time expiration
			$query = $this->db->query("DELETE FROM {$verify_table} WHERE created_at < DATE_SUB('{$now}', INTERVAL 5 MINUTE)");

			// Query execution
			return $query;
		}


        public function delete_temp_data($uuid)
        {
              // Query that looking for code and time expiration
              $query_one = $this->db->query("DELETE FROM verify WHERE temp_uuid = '{$uuid}'");
              $query_two = $this->db->query("DELETE FROM calculator WHERE uuid = '{$uuid}'");

              if($query_one && $query_two) {
                  return TRUE;
              }
        }

        /**
         * @param $temp_lead
         * @param $calc
         *
         * @return string - new uuid
         */
        public function insert_lead($temp_lead, $calc)
		{
		    if($calc["active_loan"] == "fcl") {
		        $amount = $calc["amount_fcl"];
		        $months = $calc["months_fcl"];
		        $annuity = $calc["annuity_fcl"];
            } else {
                $amount = $calc["amount_two_in_one"];
                $months = $calc["months_two_in_one"];
                $annuity = $calc["annuity_two_in_one"];
            }

			$this->insert_row("leads", [
                "firstname",
                "lastname",
                "email",
                "mobile",
                "uuid",
                "uuri",
                "otp",
                'jmbg',
                "expositure",
                "general_consent",
                "kreditni_biro_consent",
                "amount",
                "months",
                "annuity",
                "amount_fcl",
                "months_fcl",
                "annuity_fcl",
                "amount_two_in_one",
                "months_two_in_one",
                "annuity_two_in_one",
                "loan_type",
                "recaptcha_confidence",
                "ip"
            ], [
                $temp_lead["firstname"],
                $temp_lead["lastname"],
                $temp_lead["email"],
                $temp_lead["mobile"],
                $temp_lead["temp_uuid"],
                html_escape(addiko_crypt($temp_lead['email'])),
                $temp_lead["otp"],
                $temp_lead["jmbg"],
                $temp_lead["expositure"],
                $temp_lead["general_consent"],
                $temp_lead["kreditni_biro_consent"],
                $amount,
                $months,
                $annuity,
                $calc["amount_fcl"],
                $calc["months_fcl"],
                $calc["annuity_fcl"],
                $calc["amount_two_in_one"],
                $calc["months_two_in_one"],
                $calc["annuity_two_in_one"],
                $calc["active_loan"],
                $temp_lead["recaptcha_confidence"],
                $temp_lead["ip"]
            ]);

			return $temp_lead["temp_uuid"];
		}

		public function get_branch_office($expositure) {
            return $this->get_row("SELECT * FROM office WHERE id=?", [$expositure]);
        }


        /**
         * @param $lead
         * @return mixed
         */
        public function insert_lead_to_mautic($lead)
		{
			$data = array(
				'firstname'                 => html_escape($lead['firstname']),
				'lastname'                  => html_escape($lead['lastname']),
				'email'                     => html_escape($lead['email']), // Get user mail
				'mobile'                    => html_escape($lead['mobile']), // Get user mobile

//				'contactcenter'             => "", // Get user mobile

				'uuid'                      => html_escape($lead['uuid']),
				'uuri'                      => html_escape(addiko_crypt($lead['email'])),

				'channel_asistirana'        => "yes",  // consent za letnu nagradnu igru (iako izgleda da je kanal, u pitanju je consent)

                'consent_general'           => ($lead["general_consent"] == 1)?"yes":"no",
                'consent_asistirana'        => "yes"

//                'hotlead_asistirana'        => "no"
			);

            // Call Mautic
            $mautic = new Mautic();
            $mautic->create_lead($data);

            return $lead[ADDIKO_TEMP_USER_UUID];
		}

        public function get_lead_by_uuid($uuid) {
            return $this->get_row("SELECT * FROM leads WHERE uuid=?", [$uuid]);
        }

        public function get_lead_by_uuri($uuri) {
            return $this->get_row("SELECT * FROM leads WHERE uuri=?", [$uuri]);
        }

        public function unsubscribe($email, $field) {
            $lead = $this->get_row("SELECT * FROM leads WHERE email=?", [$email]);

            $edited_date = date('Y-m-d H:i:s');

            if($lead) {
                $this->insert_row("leads_history", [
                    "lead_id",
                    "created_at",
                    "firstname",
                    "lastname",
                    "email",
                    "mobile",
                    "uuid",
                    "uuri",
                    "otp", //sms code
                    "general_consent",
                    "general_consent_mail",
                    "general_consent_sms",
                    "general_consent_phone",
                    "prize_game_consent",
                    "prize_game_consent_mail",
                    "prize_game_consent_sms",
                    "prize_game_consent_phone",
                    "brit_awards_consent_mail",
                    "brit_awards_consent_sms",
                    "brit_awards_consent_phone",
                    "income",
                    "segmentation",
                    "recaptcha_confidence",
                    "ip"
                ], [
                    $lead["id"],
                    $lead["created_at"],
                    $lead["firstname"],
                    $lead["lastname"],
                    $lead["email"],
                    $lead["mobile"],
                    $lead["uuid"],
                    $lead['email'],
                    $lead["otp"],
                    $lead["general_consent"],
                    $lead["general_consent_mail"],
                    $lead["general_consent_sms"],
                    $lead["general_consent_phone"],
                    $lead["prize_game_consent"],
                    $lead["prize_game_consent_mail"],
                    $lead["prize_game_consent_sms"],
                    $lead["prize_game_consent_phone"],
                    $lead["brit_awards_consent_mail"],
                    $lead["brit_awards_consent_sms"],
                    $lead["brit_awards_consent_phone"],
                    $lead["income"],
                    $lead["segmentation"],
                    $lead["recaptcha_confidence"],
                    $lead["ip"]
                ]);

                $this->update_row("leads", $lead["id"],
                    [
                        "last_edited_at",
                        $field,
                        "ip"
                    ], [
                        $edited_date,
                        0,
                        $_SERVER['REMOTE_ADDR']
                    ]
                );

                return true;
            } else {
                return false;
            }
        }


        /**
         * Send email to the user using mautic
         *
         * @param $uuid
         */
        public function send_mail($uuid)
		{
			$uuid = html_escape($uuid);

			$user = $this->db->query("SELECT * FROM leads WHERE uuid='{$uuid}'")->row();

			$addiko_mailer = ADDIKO_MAILER;

			if (isset($user)) {
				$email     = $user->email;
				// $firstname = $user->firstname;
			} else {
				$email     = '';
				// $firstname = '';
				return;
			} // end if

			$mautic = new Mautic();
			$mautic->facebook_share($email, 13);
		}

		public function get_offices() {
            return $this->get_array("SELECT * FROM office");
        }

	} // end class


} // end if
