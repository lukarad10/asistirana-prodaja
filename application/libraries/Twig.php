<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Library to wrap Twig layout engine. Originally from Bennet Matschullat.
 * Code cleaned up to CodeIgniter standards by Erik Torsner
 *
 * PHP Version 5.3
 *
 * @category Layout
 * @package  Twig
 * @author   Bennet Matschullat <bennet@3mweb.de>
 * @author   Erik Torsner <erik@torgesta.com>
 * @license  Don't be a dick http://www.dbad-license.org/
 * @link     https://github.com/bmatschullat/Twig-Codeigniter
 */

/**
 * Main (and only) class for the Twig wrapper library
 *
 * @category Layout
 * @package  Twig
 * @author   Bennet Matschullat <hello@bennet-matschullat.com>
 * @author   Erik Torsner <erik@torgesta.com>
 * @license  Don't be a dick http://www.dbad-license.org/
 * @link     https://github.com/bmatschullat/Twig-Codeigniter
 */
class Twig {
    const 		TWIG_CONFIG_FILE = 'twig';

    /**
     * Path to module folder. If it is empty string, that means that controller that is calling is in
     * application/controllers folder
     *
     * @var string
     */
    public $module;

    /**
     * Path to templates. Usually application/views.
     *
     * @var string
     */
    protected $template_dir;

    /**
     * Path to cache.  Usually applcation/cache.
     *
     * @var string
     */
    protected $cache_dir;

    /**
     * Reference to code CodeIgniter instance.
     *
     * @var CodeIgniter object
     */
    private $_ci;

    /**
     * Twig environment see http://twig.sensiolabs.org/api/v1.8.1/Twig_Environment.html.
     *
     * @var Twig_Envoronment object
     */
    private $_twig_env;

    /**
     * constructor of twig ci class
     */
    public function __construct()
    {
        $this->_ci = & get_instance();
        $this->_ci->config->load(self::TWIG_CONFIG_FILE); // load config file


        // set include path for twig
        ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR . APPPATH . 'third_party/Twig/lib/Twig');
        require_once (string)'Autoloader.php';
        // register autoloader
        Twig_Autoloader::register();
        log_message('debug', 'twig autoloader loaded');

        // init paths
        $module = $this->_ci->router->module;

//        $module = str_replace(["../modules/", "controllers/"], ["", ""], $this->router->fetch_directory());


        if($module != "") $this->template_dir = APPPATH."modules/".$module."/views";
        else $this->template_dir = $this->_ci->config->item('template_dir');

        $this->cache_dir = $this->_ci->config->item('cache_dir');

        // load environment
        $loader = new Twig_Loader_Filesystem($this->template_dir, $this->cache_dir);
        $this->_twig_env = new Twig_Environment($loader, array(
            'cache' => $this->cache_dir,
            'auto_reload' => TRUE));

        // initialize functions and filters to be used within templates
        $this->ci_function_init();
    }

    /**
     * sets a path depending if it's a module or regular controller
     *
     * @param array   $paths	contains all paths where twig should search for views
     *
     * @return void
     *
     */
    public function setPaths($paths) {
        $loader = $this->_twig_env->getLoader();
        $loader->setPaths($paths);
    }

    /**
     * render a twig template file
     *
     * @param string  $template template name
     * @param array   $data	    contains all varnames
     * @param boolean $render   render or return raw?
     *
     * @return void
     *
     */
    public function render($template, $data = array(), $render = TRUE)
    {
//        echo "controller folder: ", $this->_ci->router->fetch_directory(), PHP_EOL;

//        $module = str_replace(["../modules/", "/controllers/"], ["", ""], $this->_ci->router->fetch_directory());
        $tmp_arr = explode("/", $this->_ci->router->fetch_directory());

        if(count($tmp_arr) > 1) {
            if($tmp_arr[1] == 'modules') {
                $module = $tmp_arr[2];
            }
        }

//        $module = $tmp_arr[2];

        if($module != "") $this->setPaths(APPPATH."modules/".$module."/views");
        else $this->setPaths($this->_ci->config->item('template_dir'));

        $template = $template.".".$this->_ci->config->item('template_file_ext');

        $template = $this->_twig_env->loadTemplate($template);
        log_message('debug', 'twig template loaded');

        $tmp = ($render) ? $template->render($data) : $template;
//        echo $tmp;
        return $tmp;
    }

    /**
     * Execute the template and send to CI output
     *
     * @param string $template Name of template
     * @param array  $data     Parameters for template
     *
     * @return void
     *
     */
    public function display($template, $data = array())
    {
//        $module = str_replace(["../modules/", "/controllers/"], ["", ""], $this->_ci->router->fetch_directory());
        $tmp_arr = explode("/", $this->_ci->router->fetch_directory());

        $module = '';

        if(count($tmp_arr) > 1) {
            if($tmp_arr[1] == 'modules') {
                $module = $tmp_arr[2];
            }
        }

        if($module != "") $this->setPaths(APPPATH."modules/".$module."/views");
        else $this->setPaths($this->_ci->config->item('template_dir'));

        $template = $template.".".$this->_ci->config->item('template_file_ext');
        $template = $this->_twig_env->loadTemplate($template);
        $this->_ci->output->set_output($template->render($data));
    }

    /**
     * Entry point for controllers (and the likes) to register
     * callback functions to be used from Twig templates
     *
     * @param string                 $name     name of function
     * @param Twig_FunctionInterface $function Function pointer
     *
     * @return void
     *
     */
    public function register_function($name, Twig_FunctionInterface $function)
    {
        $this->_twig_env->addFunction($name, $function);
    }

    /**
     * Registers a Global.
     *
     * New globals can be added before compiling or rendering a template;
     * but after, you can only update existing globals.
     *
     * @param string $name  The global name
     * @param mixed  $value The global value
     */
    public function addGlobal($name, $value) {
        $this->_twig_env->addGlobal($name, $value);
    }

    /**
     * Initialize standard CI functions
     *
     * @return void
     */
    public function ci_function_init() {
        $functions = $this->_ci->config->item('functions');
        for($i=0; $i<count($functions); $i++) {
            $this->_twig_env->addFunction($functions[$i], new Twig_Function_Function($functions[$i]));
        }

        $filters = $this->_ci->config->item('filters');
        for($i=0; $i<count($filters); $i++) {
            $this->_twig_env->addFilter($filters[$i], new Twig_Filter_Function($filters[$i]));
        }
    }
}

/* End of file Twig.php */
/* Location: ./libraries/Twig.php */
