<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!class_exists('Id_gen')) {
	/**
	 * ID generators.
	 *
	 * Generates IDs.
	 *
	 * @package	CodeIgniter
	 * @subpackage AddikoQuiz
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	class Id_gen {

		/**
		 * Generate universally unique identifier.
		 *
		 * Hope this work XD.
		 *
		 * @version 1.0.0
		 * @since 1.0.0
		 */
		public function generate_uuid()
		{
			return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				mt_rand(0, 0xffff),
				mt_rand(0, 0xffff),
				mt_rand(0, 0xffff),
				mt_rand(0, 0x0fff) | 0x4000,
				mt_rand(0, 0x3fff) | 0x8000,
				mt_rand(0, 0xffff),
				mt_rand(0, 0xffff),
				mt_rand(0, 0xffff)
			);
		} // end function

		/**
		 * Generate one-time password.
		 *
		 * Hope this work, too XD.
		 *
		 * @version 1.0.0
		 * @since 1.0.0
		 */
		public function generate_otp()
		{
			return sprintf('%d', mt_rand(100000, 999999));
		} // end function
	} // end class
} // end if
