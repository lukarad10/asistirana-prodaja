<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Mautic\MauticApi;
use Mautic\Auth\ApiAuth;

if (!class_exists('Mautic')) {
	/**
	 * Mautic init.
	 *
	 * Using class for basic auth and geting info. from Mautic via Mautic API
	 *
	 * @package	CodeIgniter
	 * @subpackage AddikoQuiz
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	class Mautic {

		/**
		 * Mautic user
		 *
		 * @since 1.0.0
		 * @var string
		 */
		protected $userName = MAUTIC_USER;

			/**
			 * Mautic user pass
			 *
			 * @since 1.0.0
			 * @var string
			 */
		protected $password = MAUTIC_PASS;

			/**
			 * Mautic API URL
			 *
			 * @since 1.0.0
			 * @var string
			 */
		protected $apiUrl = MAUTIC_URL . '/api';

		/**
		 * Authenticate user with mautic
		 *
		 * @version 1.0.0
		 * @since 1.0.0
		 */
		private function auth()
		{
			// ApiAuth->newAuth() will accept an array of Auth settings
			$settings = array(
				'userName' => $this->userName, // Create a new user
				'password' => $this->password  // Make it a secure password
			);

			// Initiate the auth object specifying to use BasicAuth
			$initAuth = new ApiAuth();
			$auth     = $initAuth->newAuth($settings, 'BasicAuth');

			return $auth;
		} // end function

        /**
         * Create contact in Mautic
         *
         * @version 1.0.0
         * @since 1.0.0
         * @param array $data Put data values to mautic user
         * @link https://developer.mautic.org/#create-contact
         * @return bool
         */
		public function create_lead($data = array())
		{
			// Attach user IP address to $data array
			$data['ipAddress'] = $_SERVER['REMOTE_ADDR'];
			$data['lastActive'] = date("Y-m-d H:m:i");
			$data['owner'] = 1;

			// Make API call
			$api = new MauticApi();
            try {
                $contactApi = $api->newApi('contacts', $this->auth(), $this->apiUrl);
            } catch (\Mautic\Exception\ContextNotFoundException $e) {
                return false;
            }

			// Creates contact
			$contactApi->create($data);
            return true;
		}

        /**
         * Get contact from mautic
         *
         * @version 1.0.0
         * @since 1.0.0
         * @param String $mail Search word
         * @return bool
         */
		public function getUser($mail = '')
		{
			// Make API call
			$api = new MauticApi();

			try {
                $contactApi = $api->newApi('contacts', $this->auth(), $this->apiUrl);
            } catch (\Mautic\Exception\ContextNotFoundException $e) {
			    return false;
            }

			// Creates contact
			$contact = $contactApi->getList($mail, 0, 1);

			if (empty($contact['contacts'])) {
			    return false;
			} else {
			    return true;
			}
		} // end function



        public function unsubscribeFromField($mail, $field)
        {
            // Make API call
            $api        = new MauticApi();
            try {
                $contactApi = $api->newApi('contacts', $this->auth(), $this->apiUrl);
            } catch (\Mautic\Exception\ContextNotFoundException $e) {
                return false;
            }

            // get contact
            $contact = $contactApi->getList($mail, 0, 1);
            if($contact) {
                $contact = $contact['contacts'];
                $id = array_keys($contact)[0];

                $data = [
                    $field => "no",
                    "ipAddress" => $_SERVER["REMOTE_ADDR"],
                    "lastActive" => date("Y-m-d H:m:i"),
                    "owner" => 1
                ];

                $contactApi->edit($id, $data, false);

                return true;
            } else {
                return false;
            }
        }

        public function unsubscribeLMC($mail = '')
        {
            return $this->unsubscribeFromField($mail, "channel_lmc");
        }

        public function unsubscribeBritAwards($mail = '')
        {
            return $this->unsubscribeFromField($mail, "consent_brit_awards");
        }

        /**
         * Facebook share Mautic
         *
         * @version 1.0.0
         * @since 1.0.0
         * @param string $mail Email
         * @param integer $mail_id
         * @return bool
         */
		public function facebook_share($mail = '', $mail_id = 0)
		{
			// Make API call
			$api = new MauticApi();

            try {
                $contactApi = $api->newApi('contacts', $this->auth(), $this->apiUrl);

                /**
                 * @var $emailApi \Mautic\Api\Emails
                 */
                $emailApi = $api->newApi("emails", $this->auth(), $this->apiUrl);
            } catch (\Mautic\Exception\ContextNotFoundException $e) {
                return false;
            }

			// Creates contact
			$contact = $contactApi->getList($mail, 0, 1);
			$contact = $contact['contacts'];
			$id = array_keys($contact)[0];
			$emailApi->sendToContact($mail_id, $id);

			return true;
		}
	}
}
