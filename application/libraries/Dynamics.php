<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use AlexaCRM\CRMToolkit\Client;
use AlexaCRM\CRMToolkit\KeyAttributes;
use AlexaCRM\CRMToolkit\Settings;
use AlexaCRM\CRMToolkit\Entity\MetadataCollection;
use AlexaCRM\CRMToolkit\OptionSetValue;

	/**
	 * Dymanics init.
	 *
	 * MS Dynamics SOAP
	 *
	 * @package	CodeIgniter
	 * @subpackage AddikoQuiz
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	class Dynamics {
        protected $clientOptions;
        protected $clientSettings;
        protected $client;
		protected $metadata;
		protected $contact;
		protected $contact_id;

        public function __construct()
        {
            $this->clientOptions  = include __DIR__ . '/../config/dynamics.php';
            $this->clientSettings = new Settings($this->clientOptions);
            $this->client         = new Client($this->clientSettings);
            $this->metadata       = MetadataCollection::instance($this->client);
        }

        public function createContact($firstname, $lastname, $email, $mobile)
        {
            $contact = $this->client->entity('contact');

            $contact->firstname = $firstname;
            $contact->lastname = $lastname;
            $contact->emailaddress1 = $email;
            $contact->mobilephone = $mobile;

//            if(ADDIKO_TEST) {
//                $this->contact->new_contactsource = new OptionSetValue(DYNAMICS_CONTACT_SOURCE, null);
//            } else {
//                $this->contact->fxn_contact_source = new OptionSetValue(DYNAMICS_CONTACT_SOURCE, null);
//            }

            $contact->fxn_contact_source = new OptionSetValue(DYNAMICS_CONTACT_SOURCE, null);

            $contact->fxn_marketingchannel = $this->client->entity('fxn_marketingvhannel', DYNAMICS_MARKETING_CHANNEL);
            $contact->fxn_marketingsource = $this->client->entity('fxn_marketingsource', DYNAMICS_MARKETING_SOURCE);
            $contact->fxn_SourceCampaign = $this->client->entity('campaign', DYNAMICS_SOURCE_CAMPAIGN);

            $contact->create();

            return $contact;
		}
				
		public function createConsents($fxn_purposeid, $fxn_dateofvalidity)
        {
			$gdpr = $this->client->entity('fxn_gdprconsentlog');

            $gdpr->fxn_name = 'Marketing Automation Lead';
            $gdpr->fxn_contactid = $this->contact_id;
            $gdpr->fxn_contact_email = $this->contact->emailaddress1;
            $gdpr->fxn_crm_contact = $this->client->entity('contact', $this->contact_id);

            if(DYNAMICS_NAMEN_SOGLASJA) {
                $gdpr->fxn_crmpurpose = $this->client->entity('fxn_namensoglasja', DYNAMICS_NAMEN_SOGLASJA);
            }

            $gdpr->fxn_purposeid = $fxn_purposeid;
            $gdpr->fxn_purposefields = '1';
            $gdpr->fxn_dateofacquisition = time();
            $gdpr->fxn_dateofvalidity = $fxn_dateofvalidity;
            $gdpr->fxn_source = 'Letna nagradna igra';
            $gdpr->create();
        }

        public function getContactByEmail($email)
        {
$fetchxml = <<<XML
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">
  <entity name="contact">
    <filter type="and">
      <condition attribute="emailaddress1" operator="eq" value="{$email}" />
    </filter>
  </entity>
</fetch>
XML;
            return $this->client->retrieveMultiple($fetchxml);
        }

        public function getContactByMobile($mobile)
        {
$fetchxml = <<<XML
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">
  <entity name="contact">
    <filter type="and">
      <condition attribute="mobilephone" operator="eq" value="{$mobile}" />
    </filter>
  </entity>
</fetch>
XML;
            return $this->client->retrieveMultiple($fetchxml);
        }

        public function getContactByEmailAndMobile($email, $mobile)
        {
$fetchxml = <<<XML
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">
  <entity name="contact">
    <filter type="and">
      <condition attribute="emailaddress1" operator="eq" value="{$email}" />
      <condition attribute="mobilephone" operator="eq" value="{$mobile}" />
    </filter>
  </entity>
</fetch>
XML;
            return $this->client->retrieveMultiple($fetchxml);
		}
				
		public function getGdprId($email)
        {
$fetchxml = <<<XML
<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">
  <entity name="fxn_gdprconsentlog">
    <filter type="and">
      <condition attribute="fxn_contact_email" operator="eq" value="{$email}" />
    </filter>
  </entity>
</fetch>
XML;
            return $this->client->retrieveMultiple($fetchxml);
        }
				
		public function updateContact($contact, $fxn_purposeid, $fxn_dateofvalidity) // pass user from getContactByEmail or getContactByMobile
        {
			$gdpr = $this->client->entity('fxn_gdprconsentlog');

            $gdpr->fxn_name = 'Marketing Automation Lead';
            $gdpr->fxn_contactid = $contact->contactid;
            $gdpr->fxn_contact_email = $contact->emailaddress1;
            $gdpr->fxn_crm_contact = $this->client->entity('contact', $contact->contactid);

            if(DYNAMICS_NAMEN_SOGLASJA) {
                $gdpr->fxn_crmpurpose = $this->client->entity('fxn_namensoglasja', DYNAMICS_NAMEN_SOGLASJA);
            }

            $gdpr->fxn_purposeid = $fxn_purposeid;
            $gdpr->fxn_purposefields = '1';
            $gdpr->fxn_dateofacquisition = time();
            $gdpr->fxn_dateofvalidity = $fxn_dateofvalidity;
            $gdpr->fxn_source = 'Letna nagradna igra';
            $gdpr->create();

            $contact->fxn_contact_source = new OptionSetValue(DYNAMICS_CONTACT_SOURCE, null);
            $contact->update();
        }



        // New function to facilitate consents in one place
        public function updateConsent($contact, $fxn_purposeid, $fxn_dateofvalidity, $value) // pass user from getContactByEmail or getContactByMobile
        {
            $gdpr = $this->client->entity('fxn_gdprconsentlog');

            $gdpr->fxn_name = 'Marketing Automation Lead';
            $gdpr->fxn_contactid = $contact->contactid;
            $gdpr->fxn_contact_email = $contact->emailaddress1;
            $gdpr->fxn_crm_contact = $this->client->entity('contact', $contact->contactid);

            if(DYNAMICS_NAMEN_SOGLASJA) {
                $gdpr->fxn_crmpurpose = $this->client->entity('fxn_namensoglasja', DYNAMICS_NAMEN_SOGLASJA);
            }

            $gdpr->fxn_purposeid = $fxn_purposeid;
            $gdpr->fxn_purposefields = $value;
            $gdpr->fxn_dateofacquisition = time();
            $gdpr->fxn_dateofvalidity = $fxn_dateofvalidity;
            $gdpr->fxn_source = 'Letna nagradna igra';
            $gdpr->create();

            $contact->fxn_contact_source = new OptionSetValue(DYNAMICS_CONTACT_SOURCE, null);
            $contact->update();
        }
    }

