<?php

use infobip\api\client\SendSingleTextualSms;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\sms\mt\send\textual\SMSTextualRequest;

if(!class_exists('Sms')) {
	class Sms {

		public $sms_username;
		public $sms_password;
		public $auth;
		public $client;

		public function __construct()
            {
    		$this->sms_username = ADDIKO_SMS_USERNAME;
    		$this->sms_password = ADDIKO_SMS_PASSWORD;
    		$this->auth = new BasicAuthConfiguration($this->sms_username, $this->sms_password);
    		$this->client = new SendSingleTextualSms($this->auth);
		}

		public function send_sms($tel, $message)
            {
			$message = (string) $message;
			$requestBody = new SMSTextualRequest();
			$requestBody->setFrom(ADDIKO_SMS_SENDER);
			$requestBody->setTo([$tel]);
			$requestBody->setText($message);
			$response = $this->client->execute($requestBody);
		}

		public function send_smses($tels = [], $message)
            {
			$message = (string) $message;
			$requestBody = new SMSTextualRequest();
			$requestBody->setFrom(ADDIKO_SMS_SENDER);
			$requestBody->setTo($tels);
			$requestBody->setText($message);
			$response = $this->client->execute($requestBody);
		}
	}
}
